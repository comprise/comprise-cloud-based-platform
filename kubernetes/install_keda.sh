#!/bin/sh
helm repo add kedacore https://kedacore.github.io/charts

helm repo update

kubectl create namespace keda
helm install keda kedacore/keda --namespace keda

# install rabbitMQ
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install rabbitmq --set rabbitmq.username=user,rabbitmq.password=PASSWORD bitnami/rabbitmq
