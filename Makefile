.PHONY: docker push

IMAGE            ?= comprisedev.azurecr.io/comprise-api-service
JOB_IMAGE        ?= comprisedev.azurecr.io/comprise-job-wrapper
KALDI_JOB_IMAGE  ?= comprisedev.azurecr.io/comprise-kaldi-job-wrapper
VERSION          ?= $(shell git describe --tags --always --dirty)
TAG              ?= $(VERSION)

default: docker

docker: 
	docker build --build-arg "VERSION=$(VERSION)" -t "$(IMAGE):$(TAG)" .
	@echo 'Docker image $(IMAGE):$(TAG) can now be used.'

push: docker job_docker kaldi_job_docker
	docker push "$(IMAGE):$(TAG)"
	docker push "$(JOB_IMAGE):$(TAG)"
	docker push "$(KALDI_JOB_IMAGE):$(TAG)"

job_docker: 
	docker build --build-arg "VERSION=$(VERSION)" -t "$(JOB_IMAGE):$(TAG)" -f jobwrapper.Dockerfile .

kaldi_job_docker: 
	docker build --build-arg "VERSION=$(VERSION)" -t "$(KALDI_JOB_IMAGE):$(TAG)" -f kaldijobwrapper.Dockerfile .
