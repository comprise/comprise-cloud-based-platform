import os
import locale
import json
from argparse import ArgumentParser
from datetime import datetime


def create_text_dir(input_dir=None, output_dir=None) -> None:

    # TODO: this currently prepares only text-data-raw (i.e. all collected texts without labels)
    path = os.path.join(output_dir, "text-data-raw")
    out_file_name = os.path.join(path, "text")
    os.mkdir(path)
    with os.scandir(input_dir) as it:
        for entry in it:
            if entry.name.startswith('text-seg') and entry.is_file():
                 entry_path=os.path.join(input_dir, entry.name)
                 os.system("cat -n %s | sed 's;^\s\+;%s-;' >> %s" % (entry_path, entry.name, out_file_name))

def main() -> None:
    # creating parser object
    parser = ArgumentParser(description='Merges collected text data into single monolingual plain text file')
    parser.add_argument('-i', '--inputs', type=str, metavar='', default='/data/text', help='Input directory path')
    parser.add_argument('-o', '--outputs', type=str, metavar='', default='/data', help='Output directory path')
    args = parser.parse_args()

    # execution start
    start_time = datetime.now()

    # executing the script
    create_text_dir(args.inputs, args.outputs)

    # execution end
    end_time = datetime.now()
    print(f'Execution time: ~{(end_time - start_time).seconds}s')


if __name__ == '__main__':
    main()
