import os
import locale
import json
from argparse import ArgumentParser
from datetime import datetime


def create_kaldi_dirs(input_dir=None, output_dir=None) -> None:
    # specifying input & output directories
    inputs = input_dir
    outputs = output_dir

    # getting speech & annotation file names
    files = os.listdir(inputs)
    speech_files = []
    annotation_files = []

    for file in files:
        if file.startswith('annotation-'):
            annotation_files.append(file)
        else:
            speech_files.append(file)

    # sorting speech files
    locale.setlocale(locale.LC_ALL, "C")
    speech_files.sort(key=locale.strxfrm)

    # specifying file & folder names
    output_folders = ['kaldi-data-all', 'kaldi-data-transcribed', 'kaldi-data-untranscribed']
    file_names = ['text', 'wav.scp', 'utt2spk']
    files = {}

    # creating folders & files
    for folder in output_folders:

        if not os.path.exists(os.path.join(outputs, folder)):
            os.mkdir(os.path.join(outputs, folder))

        # to save some computation 8 Kaldi files are created, opened and stored in dictionary 'files'
        for file in file_names:
            if folder == 'kaldi-data-untranscribed' and file == 'text':
                continue
            files[folder[11:] + '_' + file] = open(os.path.join(outputs, folder, file), 'w', encoding='utf-8')

    # writing to files
    for speech_file in speech_files:
        annotation_file = 'annotation-' + speech_file

        if annotation_file in annotation_files:
            with open(os.path.join(inputs, annotation_file), 'r', encoding='utf-8') as f:
                annotation = f.read()

            # as annotations are in a json format
            annotation_dict = json.loads(annotation)
            annotation_text = annotation_dict['text'] + '\n'

            # each Kaldi file is retrieved from dictionary 'files' and augmented with relevant information
            files['transcribed_text'].write(speech_file + ' ' + annotation_text)
            files['transcribed_wav.scp'].write(speech_file + ' ' + os.path.join(inputs, speech_file) + '\n')
            files['transcribed_utt2spk'].write(speech_file + ' ' + speech_file + '\n')
        else:
            annotation_text = '<UNTRANSCRIBED>\n'
            files['untranscribed_wav.scp'].write(speech_file + ' ' + os.path.join(inputs, speech_file) + '\n')
            files['untranscribed_utt2spk'].write(speech_file + ' ' + speech_file + '\n')

        files['all_text'].write(speech_file + ' ' + annotation_text)
        files['all_wav.scp'].write(speech_file + ' ' + os.path.join(inputs, speech_file) + '\n')
        files['all_utt2spk'].write(speech_file + ' ' + speech_file + '\n')

    # closing the files
    for file in files.values():
        file.close()


def main() -> None:
    # creating parser object
    parser = ArgumentParser(description='Data processing for Kaldi')
    parser.add_argument('-i', '--inputs', type=str, metavar='', default='/data/speech', help='Input directory path')
    parser.add_argument('-o', '--outputs', type=str, metavar='', default='/data', help='Output directory path')
    args = parser.parse_args()

    # execution start
    start_time = datetime.now()

    # executing the script
    create_kaldi_dirs(args.inputs, args.outputs)

    # execution end
    end_time = datetime.now()
    print(f'Execution time: ~{(end_time - start_time).seconds}s')


if __name__ == '__main__':
    main()
