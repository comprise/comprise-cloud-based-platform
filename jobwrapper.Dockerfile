FROM python:3.7

COPY requirements.txt /api-server/
RUN pip3 install -r /api-server/requirements.txt && \
    mkdir -p /data/text && \
    mkdir -p /data/speech && \
    mkdir -p /data/add/text && \
    mkdir -p /data/add/speech && \
    mkdir -p /data/deps

# Add Tini
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

# Copy api service code
COPY src /api-server/src
# Copy utils
COPY utils /api-server/utils

WORKDIR /api-server

ENTRYPOINT ["/tini", "--", "python3", "src/job_wrapper.py"]


