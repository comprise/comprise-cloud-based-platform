# About

WebUI frontend application for COMPRISE solution. 

Software dependencies:
- nodejs
- Angular 9
- Angular CLI
- Angular material
- Docker

# Configuration

Application configuration keys are stored in three seperate files:
- `src/environments/environment.ts`
- `src/environments/environment.prod.ts`
- `src/global.settings.ts`

## Development and Production environment
By using development enviromnet with out `--prod` tag, application will use settings from `src/environments/environment.ts` file. However, by using `--prod` tag, application will replace existing `environment.ts` file content with values from `environment.prod.ts` file. This can be managed from `angular.json` Angular CLI configuration file. 

> `environment.ts` content description:
  - `production: {true or false},`
  - `clientId: {Azure AD application (Client) Id}`
  - `redirectUri: {Web UI host uri}`
  - `tenantId: {Directory (tenant) Id}`
  - `authority: {Uri for Azure AD authorization page},`
  - `webApiUrl : '{ Uri for Webservice host https://comprise-dev.tilde.com}`

## Common settings

Applications keys which are not enviroment specific, are stored in the `src/global.settings.ts/` file.

Key description:
- `ApplicationScopes` are scopes which are using to protect Web API resurces
 

# Build docker deployment

Start Docker desktop on your machine. On windows machine make sure linux cointainers are enabled.
In order to prepare production image run following commands.

Build image:
```
docker build -t comprise:prod .
```

To test the build execute `docker run` and bind your local port 4200 with dockers default port 80.
```
docker run -d -p 4200:80 comprise:prod
```

Open in web browser:
```
http://localhost:4200
```

# Development

## Comprise

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.3.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


