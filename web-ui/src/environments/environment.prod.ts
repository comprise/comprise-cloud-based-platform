export const environment = {
    production: false,
    clientId: '686fc6d3-389e-4580-bc84-d19007cebc5d',
    redirectUri: 'https://comprise-dev.tilde.com',
    tenantId: '6bcbd0dd-1b69-4ee1-a1b1-00dec7fa5a1f',
    authority:
        'https://comprisedevb2c.b2clogin.com/comprisedevb2c.onmicrosoft.com/b2c_1_default/',
    authorityForgotPassword:
    'https://comprisedevb2c.b2clogin.com/comprisedevb2c.onmicrosoft.com/B2C_1_reset/',
    webApiUrl: 'https://comprise-dev.tilde.com/v1alpha',
};
