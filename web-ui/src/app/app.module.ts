import { LayoutModule } from '@angular/cdk/layout';
import { OverlayModule } from '@angular/cdk/overlay';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import Services from '@services/index';
import {MateralModuleList} from '@root/app/material.module';

import {DatePipe} from '@root/app/shared/pipes/date.pipe';


import { ApplicationsComponent } from './comprise/applications/applications.component';
import { ApplicationComponent } from './comprise/application/application.component';
import {LoadingBarComponent} from '@root/app/common/loadingbar.component';
import {TabsComponent} from '@root/app/comprise/tabs/tabs.component';
import {ConfirmDialogComponent} from '@root/app/common/confirmdialog/confirm.dialog.component';

// Speech
import { SpeechComponent } from './comprise/speech/speech.component';
import {SpeechSegmentComponent} from '@root/app/comprise/speech/speech.segment.component';
import {ResourceSpeechComponent} from '@root/app/resource/speech/resource.speech.component';
// Text
import {TextComponent} from '@root/app/comprise/text/text.component';
import {TextUtteranceComponent} from '@root/app/comprise/text/textutterance.component';
// Model
import {ModelsComponent } from '@root/app/comprise/model/models.component';
import {ModelTypeComponent } from '@root/app/comprise/model/modeltype.component';
import {ModelComponent } from '@root/app/comprise/model/model.component';

import {ResourceModule} from '@root/app/resource/resource.module';
import {AuthModule} from './auth.module';
import { FormsModule, ReactiveFormsModule,  FormBuilder } from '@angular/forms';
import {ResourceComponent} from '@root/app/resource/resource.component';
import {AnnotationComponent} from '@root/app/resource/annotation/annotation.component';
import {ResourceTextComponent} from '@root/app/resource/text/resource.text.component';

import {AnnotatorModelComponent} from '@root/app/comprise/annotator/model/annotator.model.component';
import {AnnotatorSpeechComponent} from '@root/app/comprise/annotator/speech/annotator.speech.component';
import {AnnotatorTextComponent} from '@root/app/comprise/annotator/text/annotator.text.component';

import { NgxAudioPlayerModule } from 'ngx-audio-player';

import {NewApplicationComponent} from './comprise/application/new/new.application.component';

import {SearchComponent} from './common/search.component';
import {UploadComponent} from './common/upload.component';
import {SwaggerComponent} from './swagger/swagger.component';

@NgModule({
    declarations: [
        AppComponent,
        ApplicationsComponent,
        SpeechComponent,
        ApplicationComponent,
        LoadingBarComponent,
        TabsComponent,
        SpeechSegmentComponent,
        TextComponent,
        ModelsComponent,
        TextUtteranceComponent,
        ModelComponent,
        ModelTypeComponent,
        DatePipe,
        ConfirmDialogComponent,
        ResourceComponent,
        AnnotationComponent,
        ResourceTextComponent,
        ResourceSpeechComponent,
        AnnotatorModelComponent,
        AnnotatorSpeechComponent,
        AnnotatorTextComponent,
        NewApplicationComponent,
        SearchComponent,
        UploadComponent,
        SwaggerComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        LayoutModule,
        OverlayModule,
        HttpClientModule,
        AuthModule,
        FormsModule,
        ReactiveFormsModule,
        NgxAudioPlayerModule,
        ...MateralModuleList

    ],
    providers: [
        ...Services,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
