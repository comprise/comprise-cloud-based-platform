import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationService } from '@services/comprise/application.serivce';
import { BaseHttp } from '@services/basehttp.service';
import { BroadcastService, MsalService } from '@azure/msal-angular';
import { Logger, CryptoUtils, AuthenticationParameters } from 'msal';
import { ActionService } from '@services/action.service';
import { environment } from '@root/environments/environment';
import { isIE, ApplicationScopes } from '../global.settings';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    IsPendingRequest = true;

    constructor(
        private router: Router,
        private authService: MsalService,
        private broadcastService: BroadcastService,
        private actionService: ActionService,
        private baseHttp: BaseHttp
    ) {}

    ngOnInit() {
        const context = this;
        this.baseHttp.onIsPendingChange.subscribe((val: boolean) => {
            setTimeout(() => (context.IsPendingRequest = val));
        });

        this.broadcastService.subscribe('msal:loginSuccess', () => {
            if (
                localStorage.getItem('custom.recovery.password.flow') === 'true'
            ) {
                localStorage.setItem('custom.recovery.password.flow', 'false');
                this.authService.logout();
            } else {
                this.router.navigate(['/dashboard']);
            }
        });

        this.broadcastService.subscribe('msal:loginFailure', function (
            response
        ) {
            if (
                !!response.errorMessage &&
                response.errorMessage.indexOf('AADB2C90118') > -1
            ) {
                localStorage.setItem('custom.recovery.password.flow', 'true');
                context.authService.authority =
                    environment.authorityForgotPassword;
                context.authService.loginRedirect();
                const authParams: AuthenticationParameters = {
                    scopes: [
                        ApplicationScopes.openId,
                        ApplicationScopes.general,
                    ],
                    authority: environment.authorityForgotPassword,
                };
                context.authService.loginRedirect(authParams);
            } else {
                window.location.reload();
            }
        });

        this.authService.handleRedirectCallback((authError, response) => {
            console.log({ response });
            if (authError) {
                console.error('Redirect Error: ', authError.errorMessage);
                this.actionService.toasterWarn('Session has expired!');
                return;
            }
            console.log('Redirect Success: ', response);
        });

        this.authService.setLogger(
            new Logger(
                (logLevel, message, piiEnabled) => {
                    console.log('MSAL Logging: ', message);
                },
                {
                    correlationId: CryptoUtils.createNewGuid(),
                    piiLoggingEnabled: false,
                }
            )
        );
    }
}
