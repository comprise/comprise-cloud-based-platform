import { Component, OnInit } from '@angular/core';
import { SessionStorageService, ANNOTATOR_KEY, APPLICATION_ID } from '@services/sessionstorage.service';

@Component({
    selector: 'app-annotator-speech',
    templateUrl: './annotator.speech.component.html',
})
export class AnnotatorSpeechComponent implements OnInit {

    applicationId = '';
    annotatorKey = '';

    constructor(private sessionStorage: SessionStorageService) { }

    ngOnInit() {
        this.applicationId = this.sessionStorage.getItem(APPLICATION_ID);
        this.annotatorKey = this.sessionStorage.getItem(ANNOTATOR_KEY);
    }
}
