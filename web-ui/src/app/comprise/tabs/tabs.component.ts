import { Component, OnInit, Input } from '@angular/core';
import { Application } from '@models/application.model';

@Component({
    selector: 'app-appliction-tabs',
    templateUrl: './tabs.component.html'
})
export class TabsComponent implements OnInit {
    @Input() applicationId: string;
    @Input() annotatorKey: string;
    @Input() application: Application;
    constructor() { }

    ngOnInit(): void { }
}
