import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionService } from '@services/action.service';
import {
    ApplicationService,
    ResourceType,
} from '@services/comprise/application.serivce';
import { Application } from '@models/application.model';
import { MatDialog } from '@angular/material/dialog';
import {
    ConfirmDialogComponent,
    ConfirmDialogModel,
} from '@root/app/common/confirmdialog/confirm.dialog.component';
import { LanguageCode, FormHelperService } from '@services/form.helper.service';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';

@Component({
    selector: 'app-application',
    templateUrl: './application.component.html',
    styleUrls: ['./application.component.scss'],
})
export class ApplicationComponent implements OnInit {
    autoCompleteControl = new FormControl();
    filteredOptions: Observable<LanguageCode[]>;
    availableLanguages: LanguageCode[];
    applicationId = '';
    ApplicationInfo: Application;
    EditMode = true;
    errorMessage = '';
    constructor(
        public route: ActivatedRoute,
        private router: Router,
        public actionService: ActionService,
        public appService: ApplicationService,
        public dialog: MatDialog,
        private formHelperService: FormHelperService
    ) {
        this.availableLanguages = formHelperService.GetLanguageISO();
    }

    async ngOnInit() {
        this.filteredOptions = this.autoCompleteControl.valueChanges.pipe(
            startWith(''),
            map((value) => (typeof value === 'string' ? value : value.name)),
            map((name) =>
                name ? this._filter(name) : this.availableLanguages.slice()
            )
        );
        this.applicationId = this.route.snapshot.params['Id'];
        this.ApplicationInfo = await this.appService.GetApplication(
            this.applicationId
        );
        setTimeout(() => (this.EditMode = false));
    }

    private _filter(name: string): LanguageCode[] {
        const filterValue = name.toLowerCase();
        return this.availableLanguages.filter(
            (option) =>
                option.name.toLowerCase().indexOf(filterValue) === 0 ||
                option.code.toLowerCase().indexOf(filterValue) === 0
        );
    }

    async OnEdit(data: Application) {
        const isValid = this.validateData();
        if (isValid) {
            await this.appService.EditApplication(data);
            this.actionService.toasterInfo('Data successfully saved!');
            this.EditMode = false;
        } else {
        }
    }

    validateData(): boolean {
        this.errorMessage = '';

        if (!this.ApplicationInfo.name) {
            this.errorMessage = 'Please enter an application name!';
            return false;
        }

        if (
            !this.availableLanguages.some(
                (lang) => lang.code === this.ApplicationInfo.language
            )
        ) {
            this.errorMessage = 'Invalid language code!';
            return false;
        }

        return true;
    }

    async OnApplicationDelete() {
        // await this.appService.DeleteApplication(this.ApplicationInfo);

        const message = `Are you sure you want to Delete an application?`;

        const dialogData = new ConfirmDialogModel(
            'Delete application',
            message
        );

        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            maxWidth: '400px',
            data: dialogData,
        });
        const context = this;
        dialogRef.afterClosed().subscribe(async (dialogResult) => {
            if (dialogResult) {
                context.actionService.toasterInfo(
                    'Application has been successfully added!'
                );
                await this.appService.DeleteApplication(this.ApplicationInfo);
                this.router.navigate(['dashboard/applications']);
            }
        });
    }
}
