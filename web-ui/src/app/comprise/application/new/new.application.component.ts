import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    isDevMode,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActionService } from '@services/action.service';
import {
    ApplicationService,
    ResourceType,
} from '@services/comprise/application.serivce';
import { ApplicationMainFields } from '@models/application.model';
import { FormHelperService, LanguageCode } from '@services/form.helper.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { localizedString } from '@angular/compiler/src/output/output_ast';

@Component({
    selector: 'app-new-application',
    templateUrl: './new.application.component.html',
})
export class NewApplicationComponent implements OnInit {
    autoCompleteControl = new FormControl();
    filteredOptions: Observable<LanguageCode[]>;
    @Output() OnNewApp: EventEmitter<any> = new EventEmitter();
    errorMessage = '';

    ApplicationInfo: ApplicationMainFields = {
        annotator_key: '',
        app_key: '',
        description: '',
        name: '',
        language: '',
        share_key : ''
    };
    availableLanguages: LanguageCode[];
    EditMode = true;
    constructor(
        public route: ActivatedRoute,
        public actionService: ActionService,
        public appService: ApplicationService,
        private formHelperService: FormHelperService
    ) {
        this.availableLanguages = formHelperService.GetLanguageISO();
    }

    ngOnInit(): void {
        this.filteredOptions = this.autoCompleteControl.valueChanges.pipe(
            startWith(''),
            map((value) => (typeof value === 'string' ? value : value.name)),
            map((name) =>
                name ? this._filter(name) : this.availableLanguages.slice()
            )
        );
    }

    private _filter(name: string): LanguageCode[] {
        const filterValue = name.toLowerCase();
        return this.availableLanguages.filter(
            (option) =>
                option.name.toLowerCase().indexOf(filterValue) === 0 ||
                option.code.toLowerCase().indexOf(filterValue) === 0
        );
    }

    async OnAdd(): Promise<any> {
        this.EditMode = false;
        const isValid = this.validateData();
        if (isValid) {
            await this.appService.AddApplication(this.ApplicationInfo);
            this.ApplicationInfo = {
                annotator_key: '',
                app_key: '',
                description: '',
                name: '',
                language: '',
                share_key : ''
            };
            this.actionService.toasterInfo(
                'Application has been successfully added!'
            );
            this.OnNewApp.emit(true);
        } else {
        }
        this.EditMode = true;
    }

    validateData(): boolean {
        this.errorMessage = '';

        if (!this.ApplicationInfo.name) {
            this.errorMessage = 'Please enter an application name!';
            return false;
        }

        if (
            !this.availableLanguages.some(
                (lang) => lang.code === this.ApplicationInfo.language
            )
        ) {
            this.errorMessage = 'Invalid language code!';
            return false;
        }

        return true;
    }
}
