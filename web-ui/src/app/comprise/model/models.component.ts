import { Component, OnInit, Input } from '@angular/core';
import { ActionService } from '@services/action.service';
import { ApplicationService } from '@services/comprise/application.serivce';
import { ModelType } from '@models/model.type.model';
import { Application } from '@models/application.model';
import { AnnotatorService } from '@services/comprise/annotator.service';
import { delay } from '@services/helper.services';

@Component({
    selector: 'app-models',
    templateUrl: './models.component.html',
})
export class ModelsComponent implements OnInit {
    @Input() applicationId: string;
    @Input() annotatorKey: string; // deprecated
    @Input() isAnnotator: boolean;
    @Input() application: Application;
    ModelTypes: ModelType;
    activeLink: string;

    constructor(
        private actionService: ActionService,
        private appService: ApplicationService,
        private annotatorService: AnnotatorService
    ) {}

    async ngOnInit() {
      await this.LoadData();
    }

    async LoadData() {
        if (!!this.isAnnotator) {
            await delay(1000);
            this.ModelTypes = await this.annotatorService.GetApplicationModel(
                this.applicationId
            );
        } else {
            this.ModelTypes = await this.appService.GetApplicationModel(
                this.applicationId
            );
        }
        this.activeLink = this.ModelTypes[0];
    }
}
