import { Component, OnInit, Input } from "@angular/core";
import { ApplicationService } from "@services/comprise/application.serivce";
import { Models, Model } from "@models/models.model";
import { ActionService } from "@services/action.service";
import { Application } from "@models/application.model";
import { AnnotatorService } from "@services/comprise/annotator.service";
import { MatExpansionPanel } from "@angular/material/expansion";

@Component({
    selector: "app-model-type",
    templateUrl: "./modeltype.component.html",
})
export class ModelTypeComponent implements OnInit {
    @Input() applicationId: string;
    @Input() modelType: string;
    @Input() application: Application;
    @Input() annotatorKey: string;
    @Input() isAnnotator: boolean;

    Models: Models;
    newModel: Model | any = {};
    toTrainModel: AddTrainingModel = { app_id: "", share_key: "" };
    toTrainModelList: AddTrainingModel[] = [];

    selectedRecipeType: any;
    recipeTypes: any[];

    favoriteModels: any[] = [
        {
            DisplayName: "CommonVoiceEN",
            ApplicationId: "5ec05f9163883551daebe823",
            SharedKey: "commonvoice-en",
        },
        {
            DisplayName: "CommonVoiceFR",
            ApplicationId: "5f7ee867ff13b782c70aea0e",
            SharedKey: "commonvoice-fr",
        },
        {
            DisplayName: "CommonVoiceDE",
            ApplicationId: "5f7ee725ff13b782c70aea0d",
            SharedKey: "commonvoice-de",
        },
        {
            DisplayName: "CommonVoicePT",
            ApplicationId: "5f7713ed9406e54a21c754e7",
            SharedKey: "commonvoice-pt",
        },
        {
            DisplayName: "LSRC",
            ApplicationId: "5f802f5a4382d263452b58de",
            SharedKey: "lsrc",
        },
        {
            DisplayName: "LIEPA",
            ApplicationId: "5f85612bed2d8fee320b5e7d",
            SharedKey: "liepa",
        },
    ];

    constructor(
        private actionService: ActionService,
        private appService: ApplicationService,
        private annotatorService: AnnotatorService
    ) {}

    async ngOnInit() {
        if (!!this.isAnnotator) {
            this.Models = await this.annotatorService.GetModelInfo(
                this.applicationId,
                this.modelType
            );
        } else {
            this.Models = await this.appService.GetModelInfo(
                this.applicationId,
                this.modelType
            );
        }
        this.recipeTypes = [
            {
                DisplayName: "Base STT training",
                Value: "base",
            },
            {
                DisplayName: "Weakly-supervised STT training",
                Value: "weaksup",
            },
            {
                DisplayName: "Custom",
                Value: "",
            },
        ];
        this.selectedRecipeType = this.recipeTypes[0];
        this.newModel.recipe = this.selectedRecipeType.Value;
    }

    SelectFavorite(val: any): void {
        this.toTrainModel.app_id = val.ApplicationId;
        this.toTrainModel.share_key = val.SharedKey;
        if (!this.newModel.recipe) {
            this.newModel.recipe = "base";
        }
    }

    SelectRec(rec: any) {
        this.newModel.recipe = rec.Value;
    }

    get showCustomRecipeInput(): boolean {
        return !!this.selectedRecipeType && !this.selectedRecipeType.Value;
    }

    async trainNewModel(tab: MatExpansionPanel) {
        await this.appService.TrainNewModel(
            this.applicationId,
            this.modelType,
            this.newModel.is_mt,
            this.newModel.recipe,
            this.toTrainModelList
        );
        this.newModel = {};
        this.actionService.toasterInfo(
            "New model training process has been submitted!"
        );
        this.toTrainModelList = [];
        tab.close();
        await this.ngOnInit();
    }

    AddToTrain() {
        if (!!this.toTrainModel && !!this.toTrainModel.app_id) {
            this.toTrainModelList = [
                ...this.toTrainModelList,
                this.toTrainModel,
            ];
            this.toTrainModel = { app_id: "", share_key: "" };
        } else {
            this.actionService.toasterWarn("Invalid application Id!");
        }
    }

    DeleteToTrain(app_id: string) {
        this.toTrainModelList = this.toTrainModelList.filter(
            (x) => x.app_id !== app_id
        );
    }
}

export interface AddTrainingModel {
    app_id: string;
    share_key: string;
}
