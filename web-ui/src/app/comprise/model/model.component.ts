import { Component, OnInit, Input } from '@angular/core';
import { SpeechSegment } from '@models/speech.segments';
import { ApplicationService } from '@services/comprise/application.serivce';
import { Model } from '@models/models.model';
import { ActionService } from '@services/action.service';
import { MatDialog } from '@angular/material/dialog';
import {
    ConfirmDialogComponent,
    ConfirmDialogModel,
} from '@root/app/common/confirmdialog/confirm.dialog.component';
import { Application } from '@models/application.model';
import { AnnotatorService } from '@services/comprise/annotator.service';

@Component({
    selector: 'app-model',
    templateUrl: './model.component.html',
})
export class ModelComponent implements OnInit {
    @Input() model: Model;
    @Input() application: Application;
    @Input() annotatorKey: string;
    @Input() isAnnotator: boolean;

    EditMode = false;
    constructor(
        private appService: ApplicationService,
        private annotatorService: AnnotatorService,
        public dialog: MatDialog,
        public actionService: ActionService
    ) {}
    result = '';
    ngOnInit(): void {}

    async RemoveModel() {
        const message = `Are you sure you want to delete a model?`;

        const dialogData = new ConfirmDialogModel('Model deleting', message);

        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            maxWidth: '400px',
            data: dialogData,
        });
        const context = this;
        dialogRef.afterClosed().subscribe((dialogResult) => {
            if (dialogResult) {
                context.appService
                    .DeleteModel(
                        context.model.app_id,
                        context.model.type,
                        context.model._id
                    )
                    .then((result) => {
                        context.actionService.toasterInfo(
                            'Model has been successfully deleted!'
                        );
                    });
                context.model = null;
            }
        });
    }

    async DownloadModel() {
        let url = this.appService.ModelUrl(
            this.model.app_id,
            this.model.type,
            this.model._id
        );
        if (this.isAnnotator) {
            url = this.annotatorService.AddApplicationKeyToUrl(url);
        } else {
            url = this.annotatorService.AddKey(url, this.application.app_key);
        }
        this.actionService.DownloadFile(url);
    }

    async WeaklysPost(): Promise<void> {
        await this.appService.WeaklysPost(
            this.model.app_id,
            this.model.type,
            this.model._id
        );
        this.actionService.toasterInfo(
            'Invoke of weakly-supervised training has been submitted!'
        );
    }

    async RetrainLMPost(): Promise<void> {
        await this.appService.RetrainLMPost(
            this.model.app_id,
            this.model.type,
            this.model._id
        );
        this.actionService.toasterInfo(
            'Retrain LM on collected texts has been submitted!'
        );
    }
}
