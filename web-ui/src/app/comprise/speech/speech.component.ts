import { Component, OnInit, Input } from '@angular/core';
import { ActionService } from '@services/action.service';
import { ApplicationService } from '@services/comprise/application.serivce';
import { SpeechSegments } from '@models/speech.segments';
import { AnnotatorService } from '@services/comprise/annotator.service';
import { TotalRecordCountInPage } from '@root/global.settings';
import { Application } from '@models/application.model';
import { delay } from '@services/helper.services';

@Component({
    selector: 'app-speech',
    templateUrl: './speech.component.html',
    styleUrls: ['./speech.component.scss'],
})
export class SpeechComponent implements OnInit {
    @Input() applicationId: string;
    @Input() annotatorKey: string;
    @Input() isAnnotator: boolean;
    @Input() application: Application;

    SpeechData: SpeechSegments;

    pageCount = TotalRecordCountInPage;
    limitRecords = 0;
    hasMoreRecord = true;

    searchValue: string;

    constructor(
        public actionService: ActionService,
        public applicationService: ApplicationService,
        public annotatorService: AnnotatorService
    ) {}

    async ngOnInit() {
       await this.LoadMore();
    }

    async Reload() {
        this.pageCount = TotalRecordCountInPage;
        this.limitRecords = 0;
        if (!!this.SpeechData) {
        this.SpeechData.segments = [];
        }
        await this.LoadMore();
    }

    async LoadMore() {
        let result: SpeechSegments;
        const skip = this.pageCount * this.limitRecords;
        if (this.isAnnotator) {
            await delay(1000);
            result = await this.annotatorService.GetSpeechSegments(
                this.applicationId,
                skip,
                this.pageCount,
                this.searchValue
            );
        } else {
            result = await this.applicationService.GetSpeechSegments(
                this.applicationId,
                skip,
                this.pageCount,
                this.searchValue
            );
        }
        if (!result.segments.length) {
            this.hasMoreRecord = false;
            return;
        }
        this.hasMoreRecord = result.segments.length === this.pageCount;
        this.limitRecords += 1;
        this.AddRecords(result);
    }

    AddRecords(records: SpeechSegments) {
        if (!this.SpeechData) {
            this.SpeechData = records;
        } else {
            this.SpeechData.segments = [
                ...this.SpeechData.segments,
                ...records.segments,
            ];
        }
    }

    onSearch(searchValue: string) {
        this.searchValue = searchValue;
        this.limitRecords = 0;
        this.SpeechData = null;
        this.LoadMore();
}
}
