import { Component, OnInit, Input } from '@angular/core';
import { SpeechSegment } from '@models/speech.segments';
import { ApplicationService } from '@services/comprise/application.serivce';
import { ActionService } from '@services/action.service';
import { MatDialog } from '@angular/material/dialog';
import {
    ConfirmDialogComponent,
    ConfirmDialogModel,
} from '@root/app/common/confirmdialog/confirm.dialog.component';
import { AnnotatorService } from '@services/comprise/annotator.service';

@Component({
    selector: 'app-speech-segment',
    templateUrl: './speech.segment.component.html',
})
export class SpeechSegmentComponent implements OnInit {
    @Input() segment: SpeechSegment;
    @Input() applicationId: string;
    @Input() annotatorKey: string;
    @Input() isAnnotator: boolean;

    audioFileUrl: string;
    annotationData: string;

    constructor(
        public applicationService: ApplicationService,
        public actionService: ActionService,
        public annotatorService: AnnotatorService,
        public dialog: MatDialog
    ) {}

    async ngOnInit() {
        await this.SetAudioUrl();
        // await this.SetAnnotationUrl();
    }

    async SetAudioUrl() {
        if (this.isAnnotator) {
            const audioUrl = await this.annotatorService.AddAnnotatorKeyToUrl(
                this.segment.audio_url
            );
            this.audioFileUrl = `${audioUrl}.mp3`;
        } else {
            const audioUrl = await this.applicationService.AddAuthorizationTokentoUrl(
                this.segment.audio_url
            );
            this.audioFileUrl = `${audioUrl}.mp3`;
        }
    }

    async SetAnnotationUrl() {
        // let annotationUrl = await this.applicationService.AddAuthorizationTokentoUrl(
        //     this.segment.annotation_url
        // );
        // annotationUrl += '.json';
        // this.annotationData = await this.applicationService.DownloadSpeechAnnotation(
        //     annotationUrl
        // );
    }

    async RemoveSpeechSegment() {
        const message = `Are you sure you want to Delete speech segment?`;

        const dialogData = new ConfirmDialogModel(
            'Delete speech segment',
            message
        );

        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            maxWidth: '400px',
            data: dialogData,
        });
        const context = this;
        dialogRef.afterClosed().subscribe((dialogResult) => {
            if (dialogResult) {
                context.applicationService
                    .DeleteSpeechSegment(
                        context.applicationId,
                        context.segment.id
                    )
                    .then((result) => {
                        context.segment = null;
                        context.actionService.toasterInfo(
                            'Text utterance has been successfully deleted!'
                        );
                    });
            }
        });
    }

    async SaveAnnotation(annotation) {
        if (this.isAnnotator) {
            await this.annotatorService.UploadSpeechSegmentAnnotation(
                this.applicationId,
                this.segment.id,
                annotation
            );
        } else {
            await this.annotatorService.UploadSpeechSegmentAnnotation(
                this.applicationId,
                this.segment.id,
                annotation
            );
        }
        this.actionService.toasterInfo(
            'Speech annotation has been successfully updated!'
        );
    }
}
