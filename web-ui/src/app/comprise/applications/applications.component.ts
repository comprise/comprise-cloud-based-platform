import { Component, OnInit } from '@angular/core';
import {
    ApplicationService
} from '@services/comprise/application.serivce';
import { BroadcastService, MsalService } from '@azure/msal-angular';
import { ApplicationList } from '@models/application.list.model';
import { ActionService } from '@services/action.service';
import { TotalRecordCountInPage } from '@root/global.settings';
import { FormHelperService } from '@services/form.helper.service';

@Component({
    selector: 'app-applications',
    templateUrl: './applications.component.html',
    styleUrls: ['./applications.component.scss'],
})
export class ApplicationsComponent implements OnInit {
    ApplicationList: ApplicationList = null;

    pageCount = TotalRecordCountInPage;
    limitRecords = 0;
    searchValue = '';
    hasMoreRecord = true;


    constructor(
        private applicationService: ApplicationService
    ) {}

    async ngOnInit() {
        await this.LoadMoreApp();
    }

    async LoadMoreApp() {
        const skip = this.pageCount * this.limitRecords;
        const records = await this.applicationService.GetApplications(
            skip,
            this.pageCount,
            this.searchValue
        );
        // No more new records
        if (!!records.applications && !records.applications.length) {
            this.hasMoreRecord = false;
            return;
        }
        this.hasMoreRecord = records.applications.length === this.pageCount;
        this.limitRecords += 1;
        if (!this.ApplicationList) {
            this.ApplicationList = records;
        } else {
            this.ApplicationList.applications = [
                ...this.ApplicationList.applications,
                ...records.applications,
            ];
        }
    }

    async onSearch(searchValue) {
        this.searchValue = searchValue;
        this.limitRecords = 0;
        this.ApplicationList = null;
        this.LoadMoreApp();
    }

    async OnNewApp() {
        //  await this.LoadMoreApp();
    }
}
