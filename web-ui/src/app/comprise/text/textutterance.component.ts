import { Component, OnInit, Input } from '@angular/core';
import { SpeechSegment } from '@models/speech.segments';
import { ApplicationService } from '@services/comprise/application.serivce';
import { Utterances } from '@models/text.utterances';
import { ActionService } from '@services/action.service';
import { MatDialog } from '@angular/material/dialog';
import {
    ConfirmDialogComponent,
    ConfirmDialogModel,
} from '@root/app/common/confirmdialog/confirm.dialog.component';
import { AnnotatorService } from '@services/comprise/annotator.service';

@Component({
    selector: 'app-text-utterence',
    templateUrl: './textutterance.component.html',
})
export class TextUtteranceComponent implements OnInit {
    @Input() utterances: Utterances;
    @Input() applicationId: string;
    @Input() annotatorKey: string;
    @Input() isAnnotator: boolean;

    constructor(
        public actionService: ActionService,
        public appService: ApplicationService,
        public annotatorService: AnnotatorService,
        public dialog: MatDialog
    ) {}

    async ngOnInit() {
        // const item = await this.appService.http.get(this.utterances.annotation_url);
        //  const item = await this.appService.http.get(this.utterances.text_url);
    }

    async RemoveTextUtterances() {
        const message = `Are you sure you want to Delete text utterance?`;

        const dialogData = new ConfirmDialogModel(
            'Delete text utterance',
            message
        );

        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            maxWidth: '400px',
            data: dialogData,
        });
        const context = this;
        dialogRef.afterClosed().subscribe((dialogResult) => {
            if (dialogResult) {
                context.appService
                    .DeleteTextUtterances(
                        context.applicationId,
                        context.utterances.id
                    )
                    .then((result) => {
                        context.utterances = null;
                        context.actionService.toasterInfo(
                            'Text utterance has been successfully deleted!'
                        );
                    });
            }
        });
    }

    // async onTextSave(text: string) {
    //     await this.appService.UploadTextSegmentUtterance(
    //         this.applicationId,
    //         this.utterances.id,
    //         text
    //     );
    //     this.actionService.toasterInfo(
    //         'Text utterance has been successfully updated!'
    //     );
    // }

    async SaveAnnotation(annotation) {
        if (this.isAnnotator) {
            await this.annotatorService.UploadTextSegmentAnnotation(
                this.applicationId,
                this.utterances.id,
                annotation
            );
        } else {
            await this.appService.UploadTextSegmentAnnotation(
                this.applicationId,
                this.utterances.id,
                annotation
            );
        }
        this.actionService.toasterInfo(
            'Text annotation has been successfully updated!'
        );
    }
}
