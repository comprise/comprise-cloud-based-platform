import { Component, OnInit, Input } from '@angular/core';
import { ActionService } from '@services/action.service';
import { ApplicationService } from '@services/comprise/application.serivce';
import { TextUtterances } from '@models/text.utterances';
import { AnnotatorService } from '@services/comprise/annotator.service';
import { TotalRecordCountInPage } from '@root/global.settings';
import { Application } from '@models/application.model';
import { delay } from '@services/helper.services';

@Component({
    selector: 'app-text',
    templateUrl: './text.component.html',
})
export class TextComponent implements OnInit {
    @Input() applicationId: string;
    @Input() annotatorKey: string;
    @Input() isAnnotator: boolean;
    @Input() application: Application;

    TextUtterances: TextUtterances;

    pageCount = TotalRecordCountInPage;
    limitRecords = 0;
    hasMoreRecord = true;

    searchValue: string;

    constructor(
        public actionService: ActionService,
        public applicationService: ApplicationService,
        public annotatorService: AnnotatorService
    ) {}

    async ngOnInit() {
        await this.LoadMore();
    }

    async Reload() {
        this.pageCount = TotalRecordCountInPage;
        this.limitRecords = 0;
        if (!!this.TextUtterances) {
        this.TextUtterances.utterances = [];
        }
        await this.LoadMore();
    }

    async LoadMore() {
        let result: TextUtterances;
        const skip = this.pageCount * this.limitRecords;
        if (this.isAnnotator) {
            await delay(1000);
            result = await this.annotatorService.GetTextUtterances(
                this.applicationId,
                skip,
                this.pageCount,
                this.searchValue
            );
        } else {
            result = await this.applicationService.GetTextUtterances(
                this.applicationId,
                skip,
                this.pageCount,
                this.searchValue
            );
        }

        if (!result.utterances.length) {
            this.hasMoreRecord = false;
            return;
        }
        this.hasMoreRecord = result.utterances.length === this.pageCount;
        this.limitRecords += 1;
        this.AddRecords(result);
    }

    AddRecords(records: TextUtterances) {
        if (!this.TextUtterances) {
            this.TextUtterances = records;
        } else {
            this.TextUtterances.utterances = [
                ...this.TextUtterances.utterances,
                ...records.utterances,
            ];
        }
    }

    onSearch(searchValue: string) {
        this.searchValue = searchValue;
        this.limitRecords = 0;
        this.TextUtterances = null;
        this.LoadMore();
    }
}
