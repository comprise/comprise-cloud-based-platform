import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { isIE, ApplicationScopes } from '../global.settings';
import { environment } from '@root/environments/environment';
import { AuthorizationService } from '@services/comprise/authorize.service';

import {
    MsalModule,
    MsalInterceptor,
    MSAL_CONFIG,
    MSAL_CONFIG_ANGULAR,
    MsalService,
    MsalAngularConfiguration,
} from '@azure/msal-angular';
import { Configuration } from 'msal';

export const protectedResourceMap: [string, string[]][] = [
    [
        'https://comprise-dev.tilde.com/v1alpha/applications',
        [ApplicationScopes.general],
    ],
    ['https://comprisedev.blob.core.windows.net', [ApplicationScopes.general]]
];
function MSALConfigFactory(): Configuration {
    return {
        auth: {
            clientId: environment.clientId,
            authority: environment.authority,
            validateAuthority: false,
            redirectUri: environment.redirectUri + '/login',
            postLogoutRedirectUri: environment.redirectUri,
            navigateToLoginRequestUrl: true,
        },
        cache: {
            cacheLocation: 'localStorage',
            storeAuthStateInCookie: isIE, // set to true for IE 11
        },
    };
}

function IsResource(): boolean {
    return window.location.pathname.startsWith('/resource');
}

function MSALAngularConfigFactory(): MsalAngularConfiguration {
    const protectedResource: [string, string[]][] = (IsResource() ? [] : protectedResourceMap);
    return {
        popUp: !isIE,
        consentScopes: [ApplicationScopes.openId, ApplicationScopes.general],
        unprotectedResources: ['https://www.microsoft.com/en-us/'],
        protectedResourceMap: protectedResource,
        extraQueryParameters: {},
    };
}


function EnableMsalInterceptor(): any[] {
    const interceptor = [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: MsalInterceptor,
            multi: true,
        },
    ];
    if (!IsResource()) {
        return interceptor;
    } else {
        return [];
    }
}

@NgModule({
    imports: [MsalModule],
    providers: [
        ...EnableMsalInterceptor(),
        {
            provide: MSAL_CONFIG,
            useFactory: MSALConfigFactory,
        },
        {
            provide: MSAL_CONFIG_ANGULAR,
            useFactory: MSALAngularConfigFactory,
        },
        MsalService,
        AuthorizationService,
    ],
    bootstrap: [],
})
export class AuthModule {}
