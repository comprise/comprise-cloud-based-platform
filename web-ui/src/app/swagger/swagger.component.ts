import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-swagger',
    templateUrl: './swagger.component.html',
    styleUrls: ['./swagger.component.scss']
})
export class SwaggerComponent implements OnInit {
    constructor() { }

    ngOnInit(): void {
      setTimeout(() => {
        const ui = window['SwaggerUIBundle']({
            url: 'https://comprise-dev.tilde.com/v1alpha/.well-known/service-desc',
//                url: "https://192.168.2.73:8001/v1alpha/.well-known/service-desc",
            dom_id: '#swagger-ui',
            presets: [
            window['SwaggerUIBundle'].presets.apis,
            window['SwaggerUIStandalonePreset']

            ],
            oauth2RedirectUrl: "https://comprise-dev.tilde.com/oauth2-redirect.html",
            // layout: "StandaloneLayout"
        });

        ui.initOAuth({
            clientId: '686fc6d3-389e-4580-bc84-d19007cebc5d',
            appName: 'COMPRISE Cloud Platform Swagger',
            scopeSeparator: ' ',
        });
      }, 1000);
    }
}
