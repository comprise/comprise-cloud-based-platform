import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApplicationService } from '@services/comprise/application.serivce';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-resource-text',
    templateUrl: './resource.text.component.html',
})
export class ResourceTextComponent implements OnInit {
    @Input() url: string;
    @Input() isAnnotator: boolean;
    // @Output() save = new EventEmitter<string>();

    content: string;

    constructor(private appService: ApplicationService) {}

    async ngOnInit() {
        this.content = await this.appService.DownloadFile(this.url);
    }

    // async onSave() {
    //     if (!this.isAnnotator) {
    //         this.save.emit(this.content);
    //     }
    // }
}
