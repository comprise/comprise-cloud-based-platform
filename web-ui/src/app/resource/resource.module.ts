import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';


import {ResourceRoutingModule} from './resource-routing.module';
import {MateralModuleList} from '../material.module';

@NgModule({
    imports: [
        CommonModule,
        ResourceRoutingModule,
        ...MateralModuleList
    ],
    declarations: [ ]

})
export class ResourceModule { }
