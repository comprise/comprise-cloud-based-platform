import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ResourceComponent} from './resource.component';

import {AnnotatorModelComponent} from '@root/app/comprise/annotator/model/annotator.model.component';
import {AnnotatorSpeechComponent} from '@root/app/comprise/annotator/speech/annotator.speech.component';
import {AnnotatorTextComponent} from '@root/app/comprise/annotator/text/annotator.text.component';

const routes: Routes = [
    {
        path: ':appId/:annotatorKey/:applicationKey',
        component: ResourceComponent,
        children: [
                {
                    path: 'speech',
                    component: AnnotatorSpeechComponent,
                },
                {
                    path: 'text',
                    component: AnnotatorTextComponent,
                },
                {
                    path: 'model',
                    component: AnnotatorModelComponent,
                }
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ResourceRoutingModule {}
