import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {
    ResourceType,
    ApplicationService,
} from '@services/comprise/application.serivce';
import { AnnotatorService } from '@services/comprise/annotator.service';

@Component({
    selector: 'app-annotation',
    templateUrl: './annotation.component.html',
})
export class AnnotationComponent implements OnInit {
    @Input() url: string;
    @Input() isAnnotator: boolean;
    @Output() annotationSave: EventEmitter<string> = new EventEmitter<string>();

    annotation = '';

    constructor(
        private appService: ApplicationService,
        private annotatorService: AnnotatorService
    ) {}

    async ngOnInit() {
        let url = '';
        if (this.isAnnotator) {
            url = this.annotatorService.AddAnnotatorKeyToUrl(this.url);
        } else {
            url = await this.appService.AddAuthorizationTokentoUrl(this.url);
        }

        const result = await this.appService.DownloadFile(url);
        if (!!result && !!result.length) {
            this.annotation = JSON.parse(result)['text'];
        }
    }

    async Save() {
        this.annotationSave.emit(this.annotation);
    }
}
