import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
    SessionStorageService,
    ANNOTATOR_KEY,
    APPLICATION_ID,
    APPLICATION_KEY,
} from '@services/sessionstorage.service';
import { AnnotatorService } from '@services/comprise/annotator.service';

@Component({
    selector: 'app-resource',
    templateUrl: './resource.component.html',
})
export class ResourceComponent implements OnInit {
    selectedTab: 0 | 1 | 2 = 0;
    annotatorKey = '';
    applicationKey = '';
    applicationId = '';
    constructor(
        private activeRoute: ActivatedRoute,
        private sessionStorage: SessionStorageService,
        private annotatorService: AnnotatorService,
        private router: Router
    ) {}

    ngOnInit() {
        this.applicationId = this.activeRoute.snapshot.params['appId'];
        this.annotatorKey = this.activeRoute.snapshot.params['annotatorKey'];
        this.applicationKey = this.activeRoute.snapshot.params[
            'applicationKey'
        ];

        this.sessionStorage.setItem(ANNOTATOR_KEY, this.annotatorKey);
        this.sessionStorage.setItem(APPLICATION_ID, this.applicationId);
        this.sessionStorage.setItem(APPLICATION_KEY, this.applicationKey);
        this.InitNav();
    }

    InitNav() {
        if (window.location.href.endsWith('speech')) {
            this.GoToSpeech();
        } else if (window.location.href.endsWith('text')) {
            this.GoToText();
        } else if (window.location.href.endsWith('model')) {
            this.GoToModel();
        } else {
            this.GoToSpeech();
        }
    }

    Home() {
        window.location.href = '/';
    }

    GoToSpeech() {
        this.selectedTab = 0;
        this.router.navigate(['./speech'], { relativeTo: this.activeRoute });
    }

    GoToText() {
        this.selectedTab = 1;
        this.router.navigate(['./text'], { relativeTo: this.activeRoute });
    }

    GoToModel() {
        this.selectedTab = 2;
        this.router.navigate(['./model'], { relativeTo: this.activeRoute });
    }
}
