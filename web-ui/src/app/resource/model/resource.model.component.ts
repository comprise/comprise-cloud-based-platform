import { Component, OnInit, Input } from '@angular/core';
import {
    ResourceType,
    ApplicationService,
} from '@services/comprise/application.serivce';

@Component({
    selector: 'app-resource-model',
    templateUrl: './resource.model.component.html',
})
export class ResourceModelComponent implements OnInit {
    @Input() url: string;
    @Input() useAuthToken = false;

    constructor(
        private appService: ApplicationService
    ) {}

    ngOnInit(): void { }
}
