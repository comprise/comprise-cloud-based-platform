import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-resource-speech',
    templateUrl: './resource.speech.component.html',
})
export class ResourceSpeechComponent implements OnInit {
    @Input() audioUrl: string;
    constructor() { }

    ngOnInit(): void { }
}
