export interface SpeechSegments {
    segments: SpeechSegment[];
}

export interface SpeechSegment {
    id: string;
    audio_url: string;
    annotation_url: string;
    editMode: false;
}
