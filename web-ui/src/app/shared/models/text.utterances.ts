export interface TextUtterances {
    utterances: Utterances[];
}

export interface Utterances {
    id: string;
    text_url: string;
    annotation_url: string;
}
