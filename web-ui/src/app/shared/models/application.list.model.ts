export interface ApplicationList {
    applications: Array<Application>;
}

export interface Application {
    _id: string;
    name: string;
}

