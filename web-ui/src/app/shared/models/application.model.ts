export interface Application  {
    _id: string;
    name: string;
    language: string;
    description: string;
    owner_id: string;
    app_key: string;
    annotator_key: string;
    speech_upload_url: string;
    text_upload_url: string;
    share_key: string;
}

export interface ApplicationMainFields {
    app_key: string;
    annotator_key: string;
    name: string;
    language: string;
    description: string;
    share_key: string;

}
