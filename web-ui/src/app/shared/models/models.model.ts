export interface Models {
    models: Model[];
}

export interface Model {
    _id: string;
    is_mt: boolean;
    recipe: string;
    owner_id: string;
    app_id: string;
    type: string;
    created: string;
    trained: string;
    status: string;
    latest: string;
}
