import { BaseHttp } from '@services/basehttp.service';
import { Injectable } from '@angular/core';
import { buildWebApiUri } from '../../../../global.settings';
import { ApplicationList } from '@models/application.list.model';
import { Application } from '@models/application.model';
import { SpeechSegments, SpeechSegment } from '@models/speech.segments';
import { TextUtterances, Utterances } from '@models/text.utterances';
import { ModelType } from '@models/model.type.model';
import { Models } from '@models/models.model';
import { AuthorizationService } from '@services/comprise/authorize.service';
import { ActivatedRoute } from '@angular/router';
import {
    SessionStorageService,
    ANNOTATOR_KEY,
    APPLICATION_KEY
} from '@services/sessionstorage.service';

export enum ResourceType {
    Speech,
    Text,
    Model,
}

@Injectable()
export class AnnotatorService {
    constructor(
        public http: BaseHttp,
        private authenticationService: AuthorizationService,
        private activeRoute: ActivatedRoute,
        private sessionStorage: SessionStorageService
    ) {}

    public static applicationEndpoint = 'applications';

    // Get application info
    async GetApplication(appId: string): Promise<Application> {
        const url = `${buildWebApiUri(
            AnnotatorService.applicationEndpoint
        )}/${appId}`;
        const request = this.http.get(this.AddAnnotatorKeyToUrl(url));
        return request;
    }

    // -----------------SPEECH-----------------
    // Get List of application speech segments
    async GetSpeechSegments(
        id: string,
        skip: number,
        limit: number,
        searchValue: string
    ): Promise<SpeechSegments> {
        let url = `${buildWebApiUri(
            AnnotatorService.applicationEndpoint
        )}/${id}/speech?skip=${skip}&limit=${limit}`;
        url = this.AddAnnotatorKeyToUrl(url);
        if (!!searchValue) {
            url = `${url}&search=${searchValue}`;
        }
        const request = this.http.get(url);
        return request;
    }

    // Info for speech segment
    // Same as in segment List model
    async GetSpeechInfo(
        applicationId: string,
        segmentId: string
    ): Promise<SpeechSegment> {
        const url = `${buildWebApiUri(
            AnnotatorService.applicationEndpoint
        )}/${applicationId}/speech/${segmentId}`;
        const request = this.http.get(this.AddAnnotatorKeyToUrl(url));
        return request;
    }

    async UploadSpeechSegmentAnnotation(
        applicationId: string,
        speechSegmentId: string,
        annotation: string
    ): Promise<Utterances> {
        let url = `${buildWebApiUri(
            AnnotatorService.applicationEndpoint
        )}/${applicationId}/speech/${speechSegmentId}`;
        url = this.AddAnnotatorKeyToUrl(url);
        const request = this.http.patch(url, { text: annotation });
        return request;
    }

    // -----------------TEXT-------------------

    // Get List of application speech segments
    async GetTextUtterances(
        id: string,
        skip: number,
        limit: number,
        searchValue: string
    ): Promise<TextUtterances> {
        let url = `${buildWebApiUri(
            AnnotatorService.applicationEndpoint
        )}/${id}/text?skip=${skip}&limit=${limit}`;
        if (!!searchValue) {
            url = `${url}&search=${searchValue}`;
        }
        url = this.AddAnnotatorKeyToUrl(url);
        const request = this.http.get(url);
        return request;
    }

    // Info for text segment
    // Same as in text utterance list
    async GetTextUtterancesInfo(
        applicationId: string,
        utterancesId: string
    ): Promise<Utterances> {
        const url = `${buildWebApiUri(
            AnnotatorService.applicationEndpoint
        )}/${applicationId}/text/${utterancesId}`;
        const request = this.http.get(this.AddAnnotatorKeyToUrl(url));
        return request;
    }

    // /v1alpha/applications/{app_id}/text/{utt_id}
    async UploadTextSegmentAnnotation(
        applicationId: string,
        speechSegmentId: string,
        annotation: string
    ): Promise<Utterances> {
        let url = `${buildWebApiUri(
            AnnotatorService.applicationEndpoint
        )}/${applicationId}/text/${speechSegmentId}`;
        url = this.AddAnnotatorKeyToUrl(url);
        const request = this.http.patch(url, { text: annotation });
        return request;
    }

    // --------------MODEL -----------------------
    // Get List of Model types
    async GetApplicationModel(id: string): Promise<ModelType> {
        const url = `${buildWebApiUri(
            AnnotatorService.applicationEndpoint
        )}/${id}/models`;

        const request = this.http.get(this.AddApplicationKeyToUrl(url));
        return request;
    }

    // Get Model info by Model Type ("ASR","NLU")
    async GetModelInfo(
        applicationId: string,
        modelType: string
    ): Promise<Models> {
        const url = `${buildWebApiUri(
            AnnotatorService.applicationEndpoint
        )}/${applicationId}/models/${modelType}`;
        const request = this.http.get(this.AddApplicationKeyToUrl(url));
        return request;
    }

    // -----------------COMMON---------------------

    AddAnnotatorKeyToUrl(uri: string): string {
        const appKey = this.sessionStorage.getItem(ANNOTATOR_KEY);
        return this.AddKey(uri, appKey);
    }

    AddApplicationKeyToUrl(uri: string): string {
        const appKey = this.sessionStorage.getItem(APPLICATION_KEY);
        return this.AddKey(uri, appKey);
    }

     AddKey(uri: string, key: string) {
        let joint = '?';
        if (uri.indexOf('?') > -1) {
            joint = '&';
        }
        return `${uri}${joint}api_key=${key}`;
    }

    async DownloadFile(uri: string): Promise<any> {
        const request = this.http.getFile(uri);
        return request;
    }
}
