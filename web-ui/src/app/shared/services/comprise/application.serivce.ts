import { BaseHttp } from '@services/basehttp.service';
import { Injectable } from '@angular/core';
import { buildWebApiUri } from '../../../../global.settings';
import { ApplicationList } from '@models/application.list.model';
import { Application, ApplicationMainFields } from '@models/application.model';
import { SpeechSegments, SpeechSegment } from '@models/speech.segments';
import { TextUtterances, Utterances } from '@models/text.utterances';
import { ModelType } from '@models/model.type.model';
import { Models } from '@models/models.model';
import { AuthorizationService } from '@services/comprise/authorize.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { map } from 'rxjs/operators';

export enum ResourceType {
    Speech,
    Text,
    Model,
}

@Injectable()
export class ApplicationService {
    constructor(
        public http: BaseHttp,
        private authenticationService: AuthorizationService,
        private activeRoute: ActivatedRoute,
        private httpClient: HttpClient
    ) {}

    public static applicationEndpoint = 'applications';

    // Get list of application
    async GetApplications(
        skip: number,
        limit: number,
        searchValue: string
    ): Promise<ApplicationList> {
        const url = buildWebApiUri(ApplicationService.applicationEndpoint);
        let appUrl = `${url}?skip=${skip}&limit=${limit}`;
        if (!!searchValue) {
            appUrl = `${appUrl}&search=${searchValue}`;
        }
        const request = this.http.get(appUrl);
        return request;
    }

    // Get application info
    async GetApplication(appId: string): Promise<Application> {
        const request = this.http.get(
            `${buildWebApiUri(ApplicationService.applicationEndpoint)}/${appId}`
        );
        return request;
    }

    // Edit application meta info
    async EditApplication(application: Application) {
        const request = this.http.put(
            `${buildWebApiUri(ApplicationService.applicationEndpoint)}/${
                application._id
            }`,
            application
        );
        return request;
    }

    async AddApplication(application: ApplicationMainFields) {
        const request = this.http.post(
            `${buildWebApiUri(ApplicationService.applicationEndpoint)}`,
            application
        );
        return request;
    }

    async DeleteApplication(application: Application) {
        const request = this.http.delete(
            `${buildWebApiUri(ApplicationService.applicationEndpoint)}/${
                application._id
            }`,
            application
        );
        return request;
    }

    // -----------------SPEECH-----------------
    // Get List of application speech segments
    async GetSpeechSegments(
        id: string,
        skip: number,
        limit: number,
        searchValue: string
    ): Promise<SpeechSegments> {
        let url = `${buildWebApiUri(
            ApplicationService.applicationEndpoint
        )}/${id}/speech?skip=${skip}&limit=${limit}`;
        if (!!searchValue) {
            url = `${url}&search=${searchValue}`;
        }
        const request = this.http.get(url);
        return request;
    }

    // Info for speech segment
    // Same as in segment List model
    async GetSpeechInfo(
        applicationId: string,
        segmentId: string
    ): Promise<SpeechSegment> {
        const request = this.http.get(
            `${buildWebApiUri(
                ApplicationService.applicationEndpoint
            )}/${applicationId}/speech/${segmentId}`
        );
        return request;
    }

    async DeleteSpeechSegment(
        applicationId: string,
        speechSegmentId: string
    ): Promise<Utterances> {
        const request = this.http.delete(
            `${buildWebApiUri(
                ApplicationService.applicationEndpoint
            )}/${applicationId}/speech/${speechSegmentId}`
        );
        return request;
    }

    async UploadSpeechSegmentAnnotation(
        applicationId: string,
        speechSegmentId: string,
        annotation: string
    ): Promise<Utterances> {
        const url = `${buildWebApiUri(
            ApplicationService.applicationEndpoint
        )}/${applicationId}/speech/${speechSegmentId}`;
        const request = this.http.patch(url, { text: annotation });
        return request;
    }

    // -----------------TEXT-------------------

    // Get List of application speech segments
    async GetTextUtterances(
        id: string,
        skip: number,
        limit: number,
        searchValue: string
    ): Promise<TextUtterances> {
        let url = `${buildWebApiUri(
            ApplicationService.applicationEndpoint
        )}/${id}/text?skip=${skip}&limit=${limit}`;
        if (!!searchValue) {
            url = `${url}&search=${searchValue}`;
        }
        const request = this.http.get(url);
        return request;
    }

    // Info for text segment
    // Same as in text utterance list
    async GetTextUtterancesInfo(
        applicationId: string,
        utterancesId: string
    ): Promise<Utterances> {
        const request = this.http.get(
            `${buildWebApiUri(
                ApplicationService.applicationEndpoint
            )}/${applicationId}/text/${utterancesId}`
        );
        return request;
    }

    async DeleteTextUtterances(
        applicationId: string,
        utterancesId: string
    ): Promise<Utterances> {
        const request = this.http.delete(
            `${buildWebApiUri(
                ApplicationService.applicationEndpoint
            )}/${applicationId}/text/${utterancesId}`
        );
        return request;
    }

    // /v1alpha/applications/{app_id}/text/{utt_id}
    async UploadTextSegmentAnnotation(
        applicationId: string,
        speechSegmentId: string,
        annotation: string
    ): Promise<Utterances> {
        const url = `${buildWebApiUri(
            ApplicationService.applicationEndpoint
        )}/${applicationId}/text/${speechSegmentId}`;
        const request = this.http.patch(url, { text: annotation });
        return request;
    }

    // --------------MODEL -----------------------
    // Get List of Model types
    async GetApplicationModel(id: string): Promise<ModelType> {
        const request = this.http.get(
            `${buildWebApiUri(
                ApplicationService.applicationEndpoint
            )}/${id}/models`
        );
        return request;
    }

    // Get Model info by Model Type ("ASR","NLU")
    async GetModelInfo(
        applicationId: string,
        modelType: string
    ): Promise<Models> {
        const request = this.http.get(
            `${buildWebApiUri(
                ApplicationService.applicationEndpoint
            )}/${applicationId}/models/${modelType}`
        );
        return request;
    }

    async TrainNewModel(
        applicationId: string,
        modelType: string,
        isMT: boolean,
        recipe: string,
        additionalModels: any[]
    ): Promise<Models> {
        const url = `${buildWebApiUri(
            ApplicationService.applicationEndpoint
        )}/${applicationId}/models/${modelType}`;
        const request = this.http.post(url, {
            additional_corpora: additionalModels,
            is_mt: isMT,
            recipe: recipe,
        });
        return request;
    }

    async DownloadModel(
        applicationId: string,
        modelType: string,
        modelId: string
    ): Promise<any> {
        const request = this.http.get(
            `${buildWebApiUri(
                ApplicationService.applicationEndpoint
            )}/${applicationId}/models/${modelType}/${modelId}`
        );
        return request;
    }

    async DeleteModel(
        applicationId: string,
        modelType: string,
        modelId: string
    ): Promise<any> {
        const request = this.http.delete(
            this.ModelUrl(applicationId, modelType, modelId)
        );
        return request;
    }

    async WeaklysPost(
        applicationId: string,
        modelType: string,
        mdl_id: string
    ): Promise<any> {
        const request = this.http.post(
            `${buildWebApiUri(
                ApplicationService.applicationEndpoint
            )}/${applicationId}/models/${modelType}/${mdl_id}/weaksup`
        );
        return request;
    }

    async RetrainLMPost(
        applicationId: string,
        modelType: string,
        mdl_id: string
    ): Promise<any> {
        const request = this.http.post(
            `${buildWebApiUri(
                ApplicationService.applicationEndpoint
            )}/${applicationId}/models/${modelType}/${mdl_id}/lm`
        );
        return request;
    }

    ModelUrl(
        applicationId: string,
        modelType: string,
        modelId: string
    ): string {
        return `${buildWebApiUri(
            ApplicationService.applicationEndpoint
        )}/${applicationId}/models/${modelType}/${modelId}`;
    }

    // -----------------COMMON---------------------

    async GetAuthorizationToken(): Promise<string> {
        const token = await this.authenticationService.GetAccessToken();
        return token;
    }

    async AddAuthorizationTokentoUrl(uri: string): Promise<string> {
        const token = await this.GetAuthorizationToken();
        return `${uri}&authorization=${token}`;
    }

    async DownloadFile(uri: string): Promise<any> {
        const request = this.http.getFile(uri);
        return request;
    }

    public async UplaodFile(uri: string, data: File) {
        const token = await this.GetAuthorizationToken();
        return this.httpClient
            .put<any>(uri, data, {
                headers: {
                    'x-ms-blob-type': 'BlockBlob',
                },
            })
            .pipe(
                map((event) => {
                    if (!event) {
                        return 'Done';
                    }
                    switch (event.type) {
                        case HttpEventType.UploadProgress:
                            const progress = Math.round(
                                (100 * event.loaded) / event.total
                            );
                            return { status: 'progress', message: progress };

                        case HttpEventType.Response:
                            return event.body;
                        default:
                            return `Unhandled event: ${event.type}`;
                    }
                })
            );
    }

    public CreateResourceUrl(
        appId: string,
        annotatorKey: string,
        applicationKey: string
    ): string {
        return `${window.location.origin}/resource/${appId}/${annotatorKey}/${applicationKey}`;
    }
    public CreateResourceUrlWithType(
        appId: string,
        annotatorKey: string,
        type: ResourceType
    ): string {
        let resourceType: string;
        switch (type) {
            case ResourceType.Speech: {
                resourceType = 'speech';
                break;
            }
            case ResourceType.Text: {
                resourceType = 'text';
                break;
            }
            case ResourceType.Model: {
                resourceType = 'model';
                break;
            }
            default:
                throw new DOMException('Uknown resource type');
        }
        return `${window.location.origin}/resource/${appId}/${annotatorKey}/${resourceType}`;
    }
}
