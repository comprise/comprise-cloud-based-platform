import { Injectable } from '@angular/core';
import { BroadcastService, MsalService } from '@azure/msal-angular';
import { ApplicationScopes } from '@root/global.settings';
import {
    UserAgentApplication,
    Configuration,
    AuthenticationParameters,
    AuthResponse,
} from 'msal';
@Injectable()
export class AuthorizationService {
    constructor(
        private broadCastService: BroadcastService,
        private msalService: MsalService
    ) {}

    public async GetAccessToken(): Promise<string> {
        const params: AuthenticationParameters = {};
        params.scopes = [ApplicationScopes.general, ApplicationScopes.openId];
        const response: AuthResponse = await this.msalService.acquireTokenSilent(
            params
        );
        return response.accessToken;
    }

    public async StoreAnnotationKey(key: string) {

    }

}
