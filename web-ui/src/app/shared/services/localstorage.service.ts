import { Injectable } from '@angular/core';

interface ILocalStorage {
  getItem(key: string): string | null;
  setItem(key: string, value: object | string): void;
  clear(): void;
}

declare const localStorage: ILocalStorage;

@Injectable()
export class LocalStorageService implements ILocalStorage {
  constructor() {}

  clear(): void {
    if (!!localStorage) {
      return localStorage.clear();
    } else {

    }
  }

  getItem(key: string): string | null {
    if (!!localStorage) {
      return localStorage.getItem(key);
    } else {

      return null;
    }
  }

  setItem(key: string, value: object | string): void {
    if (!!localStorage) {
      localStorage.setItem(key, value);
    } else {

    }
  }
}
