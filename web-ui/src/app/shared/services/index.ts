import { ActionService } from '@services/action.service';
import { BaseHttp } from '@services/basehttp.service';
import { ApplicationService } from '@services/comprise/application.serivce';
import { AnnotatorService } from '@services/comprise/annotator.service';

import {LocalStorageService} from '@services/localstorage.service';
import {SessionStorageService} from '@services/sessionstorage.service';
import {FormHelperService} from '@services/form.helper.service';

const Services = [
    ActionService,
    BaseHttp,
    LocalStorageService,
    SessionStorageService,
    ApplicationService,
    AnnotatorService,
    FormHelperService
];

export default Services;
