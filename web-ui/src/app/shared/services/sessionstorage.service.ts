import { Injectable } from '@angular/core';

export const ANNOTATOR_KEY =  'annotator_key';
export const APPLICATION_KEY =  'application_key';
export const APPLICATION_ID =  'application_id';

interface IsessionStorage {
  getItem(key: string): string | null;
  setItem(key: string, value: object | string): void;
  clear(): void;
}

declare const sessionStorage: IsessionStorage;

@Injectable()
export class SessionStorageService implements IsessionStorage {
  constructor() {}

  clear(): void {
    if (!!sessionStorage) {
      return sessionStorage.clear();
    } else {

    }
  }

  getItem(key: string): string | null {
    if (!!sessionStorage) {
      return sessionStorage.getItem(key);
    } else {

      return null;
    }
  }

  setItem(key: string, value: object | string): void {
    if (!!localStorage) {
        sessionStorage.setItem(key, value);
    } else {

    }
  }
}
