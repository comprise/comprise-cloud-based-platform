import { Injectable, Inject } from '@angular/core';
import { Subscriber, Subject, BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ActionService } from './action.service';
import { LocalStorageService } from '@services/localstorage.service';

@Injectable()
export class BaseHttp {
    public requestsPending = 0;
    public onIsPendingChange: Observable<boolean>;
    private requestPendingSubscriber: BehaviorSubject<
        boolean
    > = new BehaviorSubject<boolean>(false);

    constructor(
        public http: HttpClient,
        private route: Router,
        private action: ActionService,
        private localStorage: LocalStorageService
    ) {
        this.onIsPendingChange = this.requestPendingSubscriber.asObservable();
    }

    public async get(url: string): Promise<any> {
        this.setRequestStatus();
        const source$ = this.http.get(url);
        return this.SetResponseMessage(source$);
    }

    public async getFile(url: string): Promise<any> {
        this.setRequestStatus();
        const options = {
            responseType: 'text' as const,
          };
        const source$ = this.http.get(url, options);
        return this.SetResponseMessage(source$, true);
    }

    public async postFile(url: string, body?: any): Promise<any> {
        this.setRequestStatus();
        const source$ = this.http.post(url, body);
        return this.SetResponseMessage(source$, true);
    }

    public async post(url: string, body?: any): Promise<any> {
        this.setRequestStatus();
        const source$ = this.http.post(url, body);
        return this.SetResponseMessage(source$);
    }

    public async put(url: string, body?: any): Promise<any> {
        this.setRequestStatus();
        const source$ = this.http.put(url, body);
        return this.SetResponseMessage(source$);
    }

    public async delete(url: string, body?: any): Promise<any> {
        this.setRequestStatus();
        const source$ = this.http.delete(url);
        return this.SetResponseMessage(source$);
    }

    public async patch(url: string, body?: any): Promise<any> {
        this.setRequestStatus();
        const source$ = this.http.patch(url, body);
        return this.SetResponseMessage(source$);
    }

    private setRequestStatus() {
        this.requestsPending++;
        this.requestPendingSubscriber.next(this.requestsPending > 0);
    }

    private SetResponseMessage(source: Observable<Object>, isFile: boolean = false): Promise<Object> {
        return source
            .toPromise()
            .then(
                isFile ? this.SuccessFileHandler.bind(this) : this.SuccessHandler.bind(this),
                this.ErrorHandler().bind(this)
            );
    }

    // Not json content - don't parse
    private  SuccessFileHandler (response: Response) {
        this.requestsPending--;
        this.requestPendingSubscriber.next(this.requestsPending > 0);
        return response;
    }

    private SuccessHandler(response: Response) {
        this.requestsPending--;
        this.requestPendingSubscriber.next(this.requestsPending > 0);
        let result: any = null;
        if (typeof response === 'string') {
            result = JSON.parse(response);
        } else {
            result = response;
        }
        return result;
    }

    private ErrorHandler(endpoint?: string, parameters?: any) {
        const context = this;
        return (response: any) => {
            this.requestsPending--;
            this.requestPendingSubscriber.next(this.requestsPending > 0);
            if (response.status === 401) {
                context.action.toasterError('Insufficient permissions!');
                const _route = context.route.url;
                return null;
            }
            if (response.status === 500) {
                context.action.toasterError(
                    'Error occurred. Process has been aborted!'
                );
                return null;
            }

            if (response.status === 404) {
                // context.action.toasterError(
                //     'Error occurred. Resource not found!'
                // );
                return null;
            }
                context.action.toasterWarn(response.message);


            return {};
        };
    }
}
