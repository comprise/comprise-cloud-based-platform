import { Component, Injectable, ViewContainerRef } from '@angular/core';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
declare const alert: any;

@Injectable()
export class ActionService {
    durationInSeconds = 5;
    readonly toastrConfig: MatSnackBarConfig = {};
    constructor(private toastr: MatSnackBar) {

        this.toastrConfig = new MatSnackBarConfig();
        this.toastrConfig.duration = this.durationInSeconds * 1000;
        this.toastrConfig.horizontalPosition = 'right';
        this.toastrConfig.verticalPosition = 'top';
    }

  toasterInfo(message: string) {
    const _toastrConfig: MatSnackBarConfig = this.toastrConfig;
    _toastrConfig.panelClass = ['info-toastr', 'toastr'];
    this.toastr.open(message, 'Info', _toastrConfig);
  }

  toasterWarn(message: string) {
    const _toastrConfig: MatSnackBarConfig = this.toastrConfig;
    _toastrConfig.panelClass = ['warn-toastr', 'toastr'];
    this.toastr.open(message, 'Warning', _toastrConfig);
  }

  toasterError(message: string) {
    const _toastrConfig: MatSnackBarConfig = this.toastrConfig;
    _toastrConfig.panelClass = ['error-toastr', 'toastr'];
    this.toastr.open(message, 'Error', _toastrConfig);
  }

  public TransformDate(value: any, showTime: boolean = false): any {
    if (!value) {
      return '---';
    }

    let dt = new Date(value);
    // +3 gmt = -180
    const timeZone = new Date().getTimezoneOffset() * -1;
    dt = new Date(dt.getTime() + timeZone * 60000);
    let month = dt.getMonth() + 1 + '';
    let day = dt.getDate() + '';
    let hours = '';
    let minutes = '';

    if (!!showTime) {
      hours = dt.getHours() + '';
      minutes = dt.getMinutes() + '';

      if (dt.getHours() < 10) {
        hours = '0' + hours;
      }
      hours += ':';
      if (dt.getMinutes() < 10) {
        minutes = '0' + minutes;
      }
    }

    if (dt.getMonth() + 1 < 10) {
      month = '0' + month;
    }
    if (dt.getDate() < 10) {
      day = '0' + day;
    }

    return `${day}.${month}.${dt.getFullYear()} ${hours}${minutes}`;
  }

  DownloadFile(url) {
    const win = window.open(url, '_blank');
    win.focus();
}
}
