import { Pipe, PipeTransform } from '@angular/core';
import { ActionService } from '@services/action.service';
@Pipe({ name: 'date', pure: false })
export class DatePipe implements PipeTransform {
  constructor(private actionService: ActionService) {}

  transform(value: any, showTime: boolean = false): any {
    return this.actionService.TransformDate(value, showTime);
  }
}
