import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { MsalService } from '@azure/msal-angular';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private authService: MsalService) {}

    canActivate() {
        if (!!this.authService.getAccount()) {
            return true;
        }
         this.router.navigate(['/login']);
         return false;
    }
}
