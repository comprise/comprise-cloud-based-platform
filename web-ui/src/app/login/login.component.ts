import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ApplicationService} from '@services/comprise/application.serivce';
import { HttpClient } from '@angular/common/http';
import { BroadcastService, MsalService } from '@azure/msal-angular';
import { Logger, CryptoUtils, AuthenticationParameters } from 'msal';
import { environment } from 'src/environments/environment';
import {isIE, ApplicationScopes} from '../../global.settings';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loggedIn: Boolean = false;
    loading = false;

    constructor(private router: Router,
        private applicationService: ApplicationService,
        private authService: MsalService,
        private httpClient: HttpClient,
        private broadcastService: BroadcastService) {}



    ngOnInit() {
        this.broadcastService.subscribe('msal:loginFailure', () => {
            // window.location.reload();
        });
    }

    async onLogin() {
        this.loading = true;
        const authParams: AuthenticationParameters = {
            scopes : [
                ApplicationScopes.openId,
                ApplicationScopes.general
              ],
        };
        if (isIE) {
          this.authService.loginRedirect(authParams);
        } else {
          this.authService.loginPopup(authParams);
        }
    }
}
