import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationService } from '@services/comprise/application.serivce';
import { BaseHttp } from '@services/basehttp.service';
import { BroadcastService, MsalService } from '@azure/msal-angular';
import { Logger, CryptoUtils } from 'msal';
import { ActionService } from '@services/action.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    IsPendingRequest = true;

    constructor(
        private router: Router,
        private authService: MsalService,
        private broadcastService: BroadcastService,
        private actionService: ActionService,
        private baseHttp: BaseHttp
    ) {}

    ngOnInit() {
        this.broadcastService.subscribe(
            'msal:acquireTokenFailure',
            (payload) => {
                this.actionService.toasterWarn('Session has expired!');
                this.router.navigate(['/login']);
            }
        );
    }
}
