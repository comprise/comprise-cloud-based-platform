import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import {ApplicationsComponent} from '../../comprise/applications/applications.component';
import {ApplicationComponent} from '../../comprise/application/application.component';
import {SpeechComponent} from '../../comprise/speech/speech.component';
import { AuthGuard } from '../../shared/guard/auth.guard';
import { SwaggerComponent } from '@root/app/swagger/swagger.component';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
    },
    {
        path: 'applications',
        component: ApplicationsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'application/:Id',
        component: ApplicationComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'speech',
        component: SpeechComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'text',
        component: SpeechComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'models',
        component: SpeechComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'api-documentation',
        component: SwaggerComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule {}
