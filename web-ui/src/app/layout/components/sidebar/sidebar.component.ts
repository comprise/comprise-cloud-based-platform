import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
    public showMenu: string;
    constructor() {}

    ngOnInit() {
        this.showMenu = '';
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    get IsHome(): boolean {
        return window.location.pathname === '/dashboard';
    }

    get IsApplication(): boolean {
       return window.location.pathname.startsWith('/dashboard/application');
    }

    get IsSwagger(): boolean {
        return window.location.pathname.startsWith('/dashboard/api-documentation');
    }
}
