import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { BroadcastService, MsalService } from '@azure/msal-angular';
@Component({
    selector: 'app-topnav',
    templateUrl: './topnav.component.html',
    styleUrls: ['./topnav.component.scss'],
})
export class TopnavComponent implements OnInit {
    public pushRightClass: string;
    public UserName: String = '';
    public IsAuthorized = false;

    constructor(
        public router: Router,
        private authService: MsalService
    ) {
        this.router.events.subscribe((val) => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.pushRightClass = 'push-right';
        const account = this.authService.getAccount();
        this.IsAuthorized = !!account;
        if (this.IsAuthorized) {
        this.UserName = account.name || 'Developer';

        this.authService.handleRedirectCallback((authError, response) => {
         // alert(response);
          });
        }
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    onLoggedout() {
       this.authService.logout();

    }

    Login() {
        window.location.href = 'login';
    }
}
