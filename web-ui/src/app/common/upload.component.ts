import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Input,
    ViewChild,
    ElementRef,
} from '@angular/core';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { applySourceSpanToExpressionIfNeeded } from '@angular/compiler/src/output/output_ast';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApplicationService } from '@services/comprise/application.serivce';
import { ActionService } from '@services/action.service';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styles: [``],
})
export class UploadComponent implements OnInit {
    @Input() uploadUrl: string;
    @Output() uploadFile = new EventEmitter<any>();

    form: FormGroup;
    error: any;
    uploadResponse = { status: '', message: '', filePath: '' };

    @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
    files = [];

    uploading = false;
    uploaded = false;

    constructor(
        private formBuilder: FormBuilder,
        private applicationService: ApplicationService,
        private actionService: ActionService
    ) {}

    ngOnInit() {
        this.form = this.formBuilder.group({
            avatar: [''],
        });
    }

    onFileChange(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.form.get('avatar').setValue(file);
        }
    }

    onSubmit() {
        const file: File = this.form.get('avatar').value;
        if (!file) {
            this.actionService.toasterWarn('Choose a File to Upload!');
            return;
        }
        this.uploading = true;
        const context = this;
        this.applicationService.UplaodFile(this.uploadUrl, file).then((data) =>
            data.subscribe(
                (res) => {
                    context.uploading = false;
                    context.uploaded = true;
                    context.actionService.toasterInfo(
                        'Upload completed successfully!'
                    );
                    context.uploadFile.emit(null);
                },
                (err) => (this.error = err)
            )
        );
    }
}
