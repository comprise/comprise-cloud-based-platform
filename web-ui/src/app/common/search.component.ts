import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {
    @Output() search = new EventEmitter<string>();
    value: string;
    constructor() {}

    ngOnInit(): void {}

    onSearch() {
        this.search.emit(this.value);
    }
}
