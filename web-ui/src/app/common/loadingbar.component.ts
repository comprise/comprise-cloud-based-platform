import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-loading-bar',
    template: `
  <mat-progress-bar mode="indeterminate"></mat-progress-bar>
    `
})
export class LoadingBarComponent implements OnInit {
    constructor() { }

    ngOnInit(): void { }
}
