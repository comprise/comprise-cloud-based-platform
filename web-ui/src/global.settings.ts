import {environment} from './environments/environment';

export const isIE = window.navigator.userAgent.indexOf('MSIE ') > -1 || window.navigator.userAgent.indexOf('Trident/') > -1;

export const ApplicationScopes = {
    general: 'https://comprisedevb2c.onmicrosoft.com/comprise/general',
    openId: 'openid'
};

export const buildWebApiUri = (url: string) => `${environment.webApiUrl}/${url}`;

export const TotalRecordCountInPage = 10;
