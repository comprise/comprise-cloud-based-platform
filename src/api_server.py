#!/usr/bin/env python3

# Copyright 2019 Tilde

"""
COMPRISE Cloud Platform API Service
"""
import logging
import logging.config
import os.path
import pwd
import yaml
import ssl
import uuid
import asyncio
import prometheus_client as prom


import tornado.ioloop
import tornado.options
import tornado.web
import tornado.httpserver
from tornado.platform.asyncio import AnyThreadEventLoopPolicy

import global_settings
import api_handlers 
from utils.jwt_utils import JWTValidator
import utils.roles
import storage
from jobs.job_publisher import JobPublisher

from typing import Dict, Optional, List, Any, Tuple


class Application(tornado.web.Application):    

    def __init__(self) -> None:
        from tornado.options import options

        self.num_requests_processed: int = 0

        self.load_config()

        settings = dict(
            xsrf_cookies=False,
            autoescape=None,
            acme_dir=options.acme_dir,
            etc_conf=self.etc_conf,
        )

        handlers = [
            # LetsEncrypt support
            (r"/\.well-known/acme-challenge/(.*)", tornado.web.StaticFileHandler, {'path': settings["acme_dir"]}),
            # OpenID Connect discovery
            (r"/\.well-known/openid-configuration", OpenIDDiscoveryHandler),
        ]

        # V1 API
        api_handlers.v1.register(handlers)

        tornado.web.Application.__init__(self, handlers, **settings)

        self.max_filesize = options.max_filesize
        self.max_smallfilesize = options.max_smallfilesize

        # init metrics
        self._init_metrics()

    def _init_metrics(self) -> None:
        # initialize global Prometheus metrics
        # self.jobs_received = prom.Counter("jobs_received", "Count of received and scheduled jobs", ["system"]) 
        pass

    def load_config(self) -> None:
        from tornado.options import options

        with open(options.conf, encoding="utf8") as f:
            self.etc_conf: Dict[str, Any] = yaml.safe_load(f)

        # load logging config
        if "logging" in self.etc_conf:
            logging.config.dictConfig(self.etc_conf["logging"])

        # init error rate handler
        self.err_handler = ErrorRateHandler()
        self.err_handler.setLevel(logging.WARNING)
        logger = logging.getLogger()
        logger.addHandler(self.err_handler)

        # JWT validator
        if "auth" in self.etc_conf and "JWT" in self.etc_conf["auth"]:
            self.jwt_validator = JWTValidator(self.etc_conf["auth"]["JWT"])
        else:
            self.jwt_validator = None

        # initialize roles according to config
        self.role_provider = utils.roles.init_roles(self.etc_conf.get("roles", {}))

        # initialize storage according to config
        self.storage_service = storage.init_storage(self.etc_conf["storage"])

        # initialize job publisher
        self.job_publisher = JobPublisher(self.etc_conf["queue_url"])
        self.job_publisher.run()


class OpenIDDiscoveryHandler(tornado.web.RequestHandler):

    def get(self):
        reply = self.application.jwt_validator.get_discovery_json()
        if reply:
            self.set_header("Content-Type", 'application/json; charset="utf-8"')
            self.write(reply)
        else:
            self.set_status(404)


class ErrorRateHandler(logging.Handler):
    """
    A handler that calculates the rate of warnings and errors
    """

    last_time = 0
    records = []
    max_records = 300
    calc_period = 300

    def warn_rate(self):
        return self.get_rate(logging.WARNING)

    def error_rate(self):
        return self.get_rate(logging.ERROR)

    def get_rate(self, levelno):
        ctime = time.time()
        warns = [r for r in self.records if ctime - r.created < self.calc_period and r.levelno == levelno]
        return float(len(warns) * 60) / float(self.calc_period)

    def emit(self, record):
        try:
            # filter 404 
            if record.name == "tornado.access" and record.message.startswith("404"):
                return
            # filter records that are older than calc_period
            new_records = [r for r in self.records if record.created - r.created < self.calc_period]
            self.records = new_records
            if len(self.records) == self.max_records:
                self.records.pop()
            self.records.append(record)
        except (KeyboardInterrupt, SystemExit):
            raise


def main() -> None:
    # create event loops on all threads to match python2/tornado5 behavior
    asyncio.set_event_loop_policy(tornado.platform.asyncio.AnyThreadEventLoopPolicy())

    global_settings.define_global_settings()

    logging.basicConfig(level=logging.DEBUG, format="%(levelname)8s %(asctime)s %(message)s ")
    logging.info('Starting up server')   
    
    from tornado.options import options

    tornado.options.parse_command_line()
    app = Application()

    # load conf
    with open(options.conf, encoding="utf8") as f:
        conf = yaml.safe_load(f)

    prom_port = int(conf.get('prometheus_metric_port', "0"))
    if prom_port != 0:
        logging.info('Starting up prometheus client on port %s' % prom_port)
        prom.start_http_server(prom_port)
    
    # run HTTPS server, if we have config
    if conf.get('ssl_certfile', None) and conf.get('ssl_keyfile', None):
        # start HTTPS
        ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        ssl_ctx.load_cert_chain(conf['ssl_certfile'],
                                conf['ssl_keyfile'])
        https_server = tornado.httpserver.HTTPServer(app, ssl_options=ssl_ctx)

        https_server.bind(options.ssl_port)
        https_server.start(1)

    # start HTTP
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.bind(options.port)
    http_server.start(1)

    if conf.get("run_as_user", None):
        try:
            uid = pwd.getpwnam(conf["run_as_user"])[2]
            os.setuid(uid)
            logging.info("Changed user to %s" % conf["run_as_user"])
        except Exception as e:
            logging.error("Failed to change user to %s" % conf["run_as_user"], e)
    logging.info("Starting main IOLoop")
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
