import yaml
import motor
from bson.objectid import ObjectId
import datetime

import global_settings

from typing import Dict, Any

class DB:
    class __DB:
        def __init__(self, conf = None):
            if conf:
                self.conf = conf["db"]
            else:
                from tornado.options import options

                with open(options.conf, encoding="utf8") as f:
                    self.conf: Dict[str, Any] = yaml.safe_load(f)["db"]

            # load settings, initialize connection etc
            self.client = motor.motor_tornado.MotorClient(self.conf["connectionstring"])
            self.db = self.client[self.conf["database"]]

        def __str__(self):
            return repr(self) + self.val

        async def get_application(self, app_id, share_key = None):
            filter_obj = {'_id': ObjectId(app_id)}
            if share_key:
                filter_obj['share_key'] = share_key
            app = await self.db.applications.find_one(filter_obj)
            if not app:
                raise KeyError(app_id)
            return app

        async def store_application(self, app_id, app) -> bool:
            # remove immutable _id from new documentd 
            if "_id" in app:
                del app["_id"]
            result = await self.db.applications.replace_one({'_id': ObjectId(app_id)}, app)

            if result.matched_count == 1:
                return True

            return False

        async def delete_application(self, app_id) -> None:
            result = await self.db.applications.delete_many({'_id': ObjectId(app_id)})
            if result.deleted_count == 0:
                raise KeyError("Application not found")
            return True

        async def get_application_list(self, owner_id = None, skip = 0, limit = 100, search = ""):            
            filter_obj = {}
            if owner_id:           
                filter_obj['owner_id'] = owner_id
            if search:
                filter_obj['$or'] = [{"name":{"$regex": search}}, {'description': {"$regex": search}}]

            cursor = self.db.applications.find(filter_obj,{'_id': 1, 'name': 1}, skip = skip, limit = limit)

            async for app in cursor:
               yield app

        async def create_application(self, app):
            result = await self.db.applications.insert_one(app)
            return result.inserted_id

        async def create_model(self, mdl):
            result = await self.db.models.insert_one(mdl)
            return result.inserted_id

        async def change_model_status(self, mdl_id, status):
            mdl = await self.db.models.find_one({'_id': ObjectId(mdl_id)})
            if not mdl:
                raise KeyError(mdl_id)

            # remove _id from object
            del mdl["_id"]

            mdl["status"] = status

            if status == "training.ok":
                mdl["trained"] = datetime.datetime.utcnow()

            result = await self.db.models.replace_one({'_id': ObjectId(mdl_id)}, mdl)

            if result.matched_count == 1:
                return True

            return False

        async def delete_model(self, mdl_id) -> None:
            result = await self.db.models.delete_many({'_id': ObjectId(mdl_id)})
            if result.deleted_count == 0:
                raise KeyError("Model not found")
            return True

        async def get_model(self, app_id, mdl_id):
            filter_obj = {'_id': ObjectId(mdl_id), 'app_id': app_id}
            model = await self.db.models.find_one(filter_obj)
            if not model:
                raise KeyError(mdl_id)
            return model

        async def get_latest_model(self, app_id, model_type):
            models = [i async for i in self.get_model_list(model_type, app_id, limit = 1, status = "training.ok")]
            models = list(models)
            
            if not models:
                raise KeyError("No trained models")
            return models[0]

        async def get_model_list(self, model_type, app_id = None, skip = 0, limit = 100, recipe = "", status = ""):
            filter_obj = {'type': model_type}

            if app_id:
                filter_obj['app_id'] = app_id

            if recipe:
                filter_obj['recipe'] = {"$regex": recipe}

            if status:
                filter_obj['status'] = {"$regex": status}

            await self.db.models.create_index("created") # create index if not exists (TODO: refactor?)

            cursor = self.db.models.find(filter_obj, skip = skip, limit = limit).sort([("created",-1)])
            
            i = 0
            async for app in cursor:
               if i == 0 and app['status'] == "training.ok":
                   i += 1
                   app["latest"] = True
               yield app

    instance = None
    def __init__(self, conf = None):
        if not DB.instance:
            DB.instance = DB.__DB(conf)

    def __getattr__(self, name):
        return getattr(self.instance, name)
