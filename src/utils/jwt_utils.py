import jwt
import logging
import urllib.request
import json
import base64
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPublicNumbers
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization

from typing import Dict

logger = logging.getLogger(__name__)

class InvalidAuthorizationToken(Exception):
    def __init__(self, details):
        super().__init__('Invalid authorization token: ' + details)


def ensure_bytes(key):
    if isinstance(key, str):
        key = key.encode('utf-8')
    return key


def decode_value(val):
    decoded = base64.urlsafe_b64decode(ensure_bytes(val) + b'==')
    return int.from_bytes(decoded, 'big')


def rsa_pem_from_jwk(jwk):
    return RSAPublicNumbers(
            n=decode_value(jwk['n']),
            e=decode_value(jwk['e'])
            ).public_key(default_backend()).public_bytes(
                    encoding=serialization.Encoding.PEM,
                    format=serialization.PublicFormat.SubjectPublicKeyInfo
                    )


class JWTValidator(object):

    # JWT keys
    jwks = {}

    # OpenID Connect Discovery
    _oidc_discovery_json:str = None
    _oidc_discovery_conf:Dict = None

    # Azure B2C specifics
    _score_prefix:str = ""
        
    def __init__(self, config: Dict[str, str]):
        self.options = config.get("options", None)

        self.discovery_url = config.get("discovery_url", "")

        self.valid_audiences = config.get("audiences", "") # aud
        self.valid_issuer = config.get("issuer", "") # iss

        self._scope_prefix = config.get("scope_prefix", "")

        if self.discovery_url:
            # discover JWKs
            self._discover_jwks(self.discovery_url)
        elif "jwk_json" not in config:
            # fail
            raise Exception("JWKs not configured!", "JWTValidator")
        else:
            # read JWKs from file
            self.jwks = json.load(open(config["jwk_json"], "r"))
            logger.info("Successfully loaded jwks from JSON")

    def get_discovery_conf(self) -> Dict:
        return self._oidc_discovery_conf

    def get_discovery_json(self) -> str:
        return self._oidc_discovery_json

    def get_scope_prefix(self) -> str:
        return self._scope_prefix

    def _discover_jwks(self, url):
        # request OpenID Connect Discovery json
        logger.info("Attempting to get jwks from OpenID Connect discovery = %s" % url)
        contents = urllib.request.urlopen(url).read()        
        discover = json.loads(contents)
        jwks_uri = discover["jwks_uri"]
        logger.info("Discovered jwks_uri = %s" % jwks_uri)
        self._oidc_discovery_json = contents
        self._oidc_discovery_conf = discover
        self.valid_issuer = discover["issuer"]

        # request jwks json
        contents = urllib.request.urlopen(jwks_uri).read()
        self.jwks = json.loads(contents)
        logger.info("Successfully loaded jwks using OpenID Connect Discovery")

    def get_kid(self, token):
        headers = jwt.get_unverified_header(token)
        if not headers:
            raise InvalidAuthorizationToken('missing headers')
        try:
            return headers['kid']
        except KeyError:
            raise InvalidAuthorizationToken('missing kid')

    def get_jwk(self, kid):
        for jwk in self.jwks.get('keys'):
            if jwk.get('kid') == kid:
                return jwk
        raise InvalidAuthorizationToken('kid not recognized')


    def get_public_key(self, token):
        return rsa_pem_from_jwk(self.get_jwk(self.get_kid(token)))

    def validate_jwt(self, jwt_to_validate):
        public_key = self.get_public_key(jwt_to_validate)
        decoded = jwt.decode(jwt_to_validate,
                         public_key,
                         verify=True,
                         algorithms=['RS256'],
                         audience=self.valid_audiences,
                         issuer=self.valid_issuer)
        
        # if we get here, the JWT is validated
        return decoded
