from functools import wraps
import logging


def oidc_protected(required_roles: list = [], required_scopes: list = [], fail_on_empty: bool = True):
    def decorator(f):
        @wraps(f)
        async def wrapper(*args, **kwds):
            self = args[0]

            jwt_token = self.request.headers.get('authorization', "")
            if jwt_token:
                jwt_token = jwt_token.split()[-1] # take last part of the token (skips "Bearer")
            if not self.auth_disabled and (fail_on_empty or jwt_token):
                try:

                    # decode and verify token
                    payload = self.application.jwt_validator.validate_jwt(jwt_token)

                    self.client_id = payload.get("sub", "")
                    self.client_name = payload.get("user_name", "")
                    self.client_scopes = payload.get("scp", [])
                    self.client_roles = await self.application.role_provider.get_roles(self.client_id)
                    if isinstance(self.client_scopes, str):
                        self.scopes = self.client_scopes.split()

                    for scope in required_scopes:
                        if scope not in self.client_scopes:
                            raise NotAuthorizedError("insufficient permissions",
                                                     self.client_id,
                                                     self.client_name,
                                                     self.client_scopes,
                                                     required_scopes)

                    for role in required_roles:
                        if role not in self.client_roles:
                            raise NotAuthorizedError("insufficient permissions",
                                                     self.client_id,
                                                     self.client_name,
                                                     self.client_roles,
                                                     required_roles)

                except NotAuthorizedError as e:
                    logging.warning(e)
                    self.set_status(401)
                    # error messages shall be allowed for all origins
                    self.set_header('Access-Control-Allow-Origin', "*")
                    self.set_header("Content-Type", "text/plain")
                    self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
                    self.write("Insufficient permissions")
                    self.flush()
                    self.finish()
                    return None

                except Exception as e:
                    logging.warning("Client failed to authenticate: {}".format(e), exc_info=True)
                    self.set_status(401)
                    # error messages shall be allowed for all origins
                    self.set_header('Access-Control-Allow-Origin', "*")
                    self.set_header("Content-Type", "text/plain")
                    if self.request.headers.get("Authorization", ""):
                        self.set_header('WWW-Authenticate', 'Bearer, error="invalid_token", error_description="%s"'%repr(e))
                    else:                                            
                        self.set_header('WWW-Authenticate', 'Bearer')
                    self.write("Invalid token")
                    self.flush()
                    self.finish()
                    return None
            else:
                self.client_id = ""
                self.client_name = ""
                self.client_scopes = ""
                self.client_roles = []

            return await f(*args, **kwds)

        return wrapper

    return decorator


class NotAuthorizedError(Exception):
    def __init__(self, message, client_id, user_name, provided_scopes: list, required_scopes: list):
        super().__init__(message)
        self.client_id = client_id
        self.user_name = user_name
        self.provided_scopes = provided_scopes
        self.required_scopes = required_scopes

    @property
    def identity(self):
        if self.client_id and self.user_name:
            return "{} [{}]".format(self.client_id, self.user_name)
        elif self.client_id:
            return self.client_id
        else:
            return "unknown"

    def __str__(self):
        return "Access denied for client {}. Provided scopes: {} but requires {}".format(self.identity,
                                                                                         self.provided_scopes,
                                                                                         self.required_scopes)
