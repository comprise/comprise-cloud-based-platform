"""
User role providers
"""
import os
import yaml
import importlib
import logging

from typing import Dict, List

class RoleProvider(object):
    """
    Interface for Role providers
    """

    def get_roles(self, user_id: str) -> List[str]:
        raise NotImplementedError()

    def initialize(self, conf: dict):
        raise NotImplementedError()

    def __init__(self, conf: dict):
        self.initialize(conf)


class AzureADRoles(RoleProvider):
    """
    Get user roles from Azure AD
    """

    def initialize(self, conf: dict):

        # token saver function
        # saves new token after refresh
        async def token_save(token, access_token=None):
           self.client.token = token

        # prepare OAuth2 sessions with MSGraph v1.0 API
        self._scope = "https://graph.microsoft.com/.default"
        from authlib.integrations.httpx_client import AsyncOAuth2Client
        import asyncio
        self.client = AsyncOAuth2Client(client_id=conf["client_id"],
                                        client_secret=conf["client_secret"],
                                        scope=self._scope,
                                        grant_type="client_credentials",
                                        update_token=token_save,
                                        token_endpoint=conf["token_url"])

        # fetch token 
        loop = asyncio.get_event_loop()
        # use run_until_complete to block the thread
        loop.run_until_complete(asyncio.ensure_future(self.client.fetch_token()))

        # read mapping from AD groups to COMPRISE API roles
        self.group_mapping = conf["group_map"]

    async def get_roles(self, user_id: str) -> List[str]:
        if self.client.token.is_expired():
            await self.client.ensure_active_token()

        # get groups from MSGraph and known groups to COMPRISE roles
        groups = [
                  self.group_mapping[x["id"]] for x in
                  (await self.client.get("https://graph.microsoft.com/v1.0/users/%s/memberOf" % user_id, timeout=10)).json()["value"]
                  if x["id"] in self.group_mapping
                 ]

        return groups

 
class JWTRoles(RoleProvider):
    """
    Get user roles from JWT claims
    """

    def initialize(self, conf: dict):
        raise NotImplementedError()


class FileRoles(RoleProvider):
    """
    Get user roles from file
    """

    def get_roles(self, user_id: str) -> list:
        return self._roles.get(user_id, [])

    def initialize(self, conf: dict):
        roles_file = conf.get("roles_file", 'user_roles.yaml')
        if os.path.isfile(roles_file):
            self._roles: Dict[str, list] = yaml.safe_load(open(roles_file, 'r', encoding="utf-8"))
        else:
            self._roles = {}

# static helper "factory" function for initializing RoleProvider from config file
def init_roles(config: Dict[str, str]) -> RoleProvider:
    stor_class = config.get("provider", "utils.roles.FileRoles")
    stor_mod, stor_class = stor_class.rsplit(".", 1)
    stor_class = getattr(importlib.import_module(stor_mod), stor_class)
    service = stor_class(config)

    return service
