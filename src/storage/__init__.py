import importlib

from typing import Dict

class StorageService(object):

    def __init__(self, config: Dict[str, str]):
       raise NotImplementedError

    async def create_containers(self, app_id):
       raise NotImplementedError

    def get_speech_upload_url(self, app_id):
       raise NotImplementedError

    def get_text_upload_url(self, app_id):
       raise NotImplementedError

    async def download_speech_corpus(self, app_id, out_dir):
       raise NotImplementedError

    async def download_model_component(self, app_id, model_type, mdl_id, component, out_dir, save_as = None):
       raise NotImplementedError

    async def get_speech_segments(self, app_id, page = None, limit = None):
       raise NotImplementedError

    async def get_text_segments(self, app_id, page = None, limit = None):
       raise NotImplementedError

    async def delete_speech_segments(self, app_id, segments):
       raise NotImplementedError

    async def get_speech_segment(self, app_id, utt_id):
       raise NotImplementedError

    async def annotate_text_segment(self, app_id, utt_id, annotation_blob):
       raise NotImplementedError

    async def annotate_speech_segment(self, app_id, utt_id, annotation_blob):
       raise NotImplementedError

    def get_model_download_url(self, app_id, model_type, mdl_id):
       raise NotImplementedError

    def get_model_logs_download_url(self, app_id, model_type, mdl_id):
       raise NotImplementedError

    def get_model_valid_download_url(self, app_id, model_type, mdl_id):
       raise NotImplementedError

    def get_model_component_download_url(self, app_id, model_type, mdl_id, component):
       raise NotImplementedError

    async def upload_model(self, app_id, model_type, mdl_id, blob):
       raise NotImplementedError

    async def upload_model_logs(self, app_id, model_type, mdl_id, blob):
       raise NotImplementedError

    async def upload_model_valid(self, app_id, model_type, mdl_id, blob):
       raise NotImplementedError

    async def upload_model_component(self, app_id, model_type, mdl_id, component, blob):
       raise NotImplementedError

    async def delete_model(self, app_id, model_type, mdl_id):
       raise NotImplementedError


def init_storage(config: Dict[str, str]) -> StorageService:
    stor_class = config.get("provider", "storage.azure_storage.AZStorage")
    stor_mod, stor_class = stor_class.rsplit(".", 1)
    stor_class = getattr(importlib.import_module(stor_mod), stor_class)
    service = stor_class(config)

    return service
