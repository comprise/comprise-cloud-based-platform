from datetime import datetime, timedelta
import logging
import uuid

from azure.storage.blob.aio import ContainerClient
from azure.storage.blob import generate_blob_sas, BlobSasPermissions
from azure.storage.blob._shared.response_handlers import PartialBatchErrorException
import azure.core.exceptions
from storage import StorageService

from typing import Dict


class AZStorage(StorageService):

    def __init__(self, config:Dict[str, str]):
       self.AZURE_ACC_URL = config["acc_url"]
       self.AZURE_ACC_KEY = config["acc_key"]

    async def create_containers(self, app_id):
        try:
            await self.create_container("speech-%s" % app_id)
        except azure.core.exceptions.ResourceExistsError:
            pass # container already exists, skip 
        try:
            await self.create_container("text-%s" % app_id)
        except azure.core.exceptions.ResourceExistsError:
            pass # container already exists, skip 
        try:
            await self.create_container("models-%s" % app_id)
        except azure.core.exceptions.ResourceExistsError:
            pass # container already exists, skip 

    async def create_container(self, container):
        async with  ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client:
            await client.create_container()

    def _generate_sas_upload_url(self, container, blob):
        client = ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY)
        
        sas_token = generate_blob_sas(
            client.account_name,
            client.container_name,
            blob,
            permission = BlobSasPermissions(write=True),
            account_key = client.credential.account_key,
            expiry = datetime.utcnow() + timedelta(hours=1) # TODO: revise expiry date
        )

        return "%s/%s/%s?%s" % (self.AZURE_ACC_URL, container, blob, sas_token)

    def _generate_sas_download_url(self, container, blob):
        client = ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY)
        
        sas_token = generate_blob_sas(
            client.account_name,
            client.container_name,
            blob,
            permission = BlobSasPermissions(read=True),
            account_key = client.credential.account_key,
            expiry = datetime.utcnow() + timedelta(hours=1) # TODO: revise expiry date
        )

        return "%s/%s/%s?%s" % (self.AZURE_ACC_URL, container, blob, sas_token)

    async def download_speech_corpus(self, app_id, out_dir):
        container = "speech-%s" % app_id
        await self._download_all_items(container, out_dir)

    async def download_text_corpus(self, app_id, out_dir):
        container = "text-%s" % app_id
        await self._download_all_items(container, out_dir)

    async def download_model_component(self, app_id, model_type, mdl_id, component, out_dir, save_as = None):
        container = "models-%s" % app_id
        name = "%s-%s-%s" % (model_type, mdl_id, component)
        await self._download_item(container, out_dir, name, save_as)

    def get_speech_upload_url(self, app_id):
        container = "speech-%s" % app_id
        blob = "speech-seg-%s" % uuid.uuid4().hex 
         
        return self._generate_sas_upload_url(container, blob)

    def get_text_upload_url(self, app_id):
        container = "text-%s" % app_id
        blob = "text-seg-%s" % uuid.uuid4().hex 
         
        return self._generate_sas_upload_url(container, blob)

    def get_model_logs_download_url(self, app_id, model_type, mdl_id):       
        container = "models-%s" % app_id
        mdl_id = "logs-%s-%s" % (model_type, mdl_id)

        return self._generate_sas_download_url(container, mdl_id)

    def get_model_valid_download_url(self, app_id, model_type, mdl_id):       
        container = "models-%s" % app_id
        mdl_id = "valid-%s-%s" % (model_type, mdl_id)

        return self._generate_sas_download_url(container, mdl_id)

    def get_model_component_download_url(self, app_id, model_type, mdl_id, component):
        container = "models-%s" % app_id
        mdl_id = "%s-%s-%s" % (model_type, mdl_id, component)

        return self._generate_sas_download_url(container, mdl_id)

    def get_model_download_url(self, app_id, model_type, mdl_id):       
        container = "models-%s" % app_id
        mdl_id = "%s-%s" % (model_type, mdl_id)

        return self._generate_sas_download_url(container, mdl_id)

    async def get_speech_segments(self, app_id, skip = 0, limit = 100, name_starts_with = ""):
        container = "speech-%s" % app_id
        lst = await self._list_blobs(container, skip, limit, "speech-seg-" + name_starts_with)

        for x in lst:
           x["audio_url"] = self._generate_sas_download_url(container, x["id"])
           x["annotation_url"] = self._generate_sas_download_url(container, "annotation-"+x["id"])

        return lst

    async def get_text_segments(self, app_id, skip = 0, limit = 100, name_starts_with = ""):
        container = "text-%s" % app_id
        lst = await self._list_blobs(container, skip, limit, "text-seg-" + name_starts_with)

        for x in lst:
           x["text_url"] = self._generate_sas_download_url(container, x["id"])
           x["annotation_url"] = self._generate_sas_download_url(container, "annotation-"+x["id"])

        return lst

    async def _list_blobs(self, container, skip = 0, limit = 100, name_starts_with = None):
        batch_size = 5000
        limit = batch_size-skip if limit > batch_size else limit
        async with ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client:
            blobs = client.list_blobs(name_starts_with=name_starts_with, results_per_page=batch_size)
            batch_no = skip // batch_size
            skip = skip % batch_size
            # implement iteration through batches and getting correct page
            batch = 0
            async for page in blobs.by_page():
                if batch == batch_no:
                    return [{"id": blob.name} async for blob in page][skip:skip+limit]

                batch += 1
            return []

    async def _download_all_items(self, container, out_dir, name_starts_with = None):
        async with ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client:
            blobs = client.list_blobs(name_starts_with = name_starts_with)

            async for blob in blobs:
                downloader = await client.download_blob(blob.name)
                await downloader.readinto(open('%s/%s' % (out_dir, blob.name), 'wb'))

    async def _download_item(self, container, out_dir, blob_name, save_as = None):
        save_as = blob_name if save_as is None else save_as
        async with ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client:
            downloader = await client.download_blob(blob_name)
            await downloader.readinto(open('%s/%s' % (out_dir, save_as), 'wb'))

    async def delete_speech_segments(self, app_id, segments):
        container = "speech-%s" % app_id
        # delete annotations together with audio
        blobs = ["annotation-"+x for x in segments]
        blobs += segments
        await self._delete_blobs(container, blobs)

    async def delete_text_segments(self, app_id, segments):
        container = "text-%s" % app_id
        # delete annotations together with text
        blobs = ["annotation-"+x for x in segments]
        blobs += segments
        await self._delete_blobs(container, segments)

    async def _delete_blobs(self, container, blobs):
        async with ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client: 
            try:
                await client.delete_blobs(*blobs)
            except PartialBatchErrorException:
                # TODO: this is ok. Maybe will be changed in future
                pass

    async def get_speech_segment(self, app_id, utt_id):
        container = "speech-%s" % app_id
        lst = await self._list_blobs(container, name_starts_with = utt_id)

        if not lst and lst[0]["id"] == utt_id:
            raise KeyError("segment not found")

        x = lst[0] # take only first result
        x["audio_url"] = self._generate_sas_download_url(container, x["id"])
        x["annotation_url"] = self._generate_sas_download_url(container, "annotation-"+x["id"])

        return x

    async def get_text_segment(self, app_id, utt_id):
        container = "text-%s" % app_id
        lst = await self._list_blobs(container, name_starts_with = utt_id)

        if not lst and lst[0]["id"] == utt_id:
            raise KeyError("segment not found")

        x = lst[0] # take only first result
        x["text_url"] = self._generate_sas_download_url(container, x["id"])
        x["annotation_url"] = self._generate_sas_download_url(container, "annotation-"+x["id"])

        return x

    async def annotate_speech_segment(self, app_id, utt_id, annotation_blob):
        container = "speech-%s" % app_id     
        lst = await self._list_blobs(container, name_starts_with = utt_id)

        if not lst and lst[0]["id"] == utt_id:
            raise KeyError("segment not found")

        utt_id = "annotation-%s" % utt_id

        async with ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client: 
            await client.upload_blob(utt_id, annotation_blob, overwrite=True)

    async def annotate_text_segment(self, app_id, utt_id, annotation_blob):
        container = "text-%s" % app_id     
        lst = await self._list_blobs(container, name_starts_with = utt_id)

        if not lst and lst[0]["id"] == utt_id:
            raise KeyError("segment not found")

        utt_id = "annotation-%s" % utt_id

        async with ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client: 
            await client.upload_blob(utt_id, annotation_blob, overwrite=True)

    async def upload_model(self, app_id, model_type, mdl_id, blob):
        container = "models-%s" % app_id
        mdl_id = "%s-%s" % (model_type, mdl_id)

        async with ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client: 
            await client.upload_blob(mdl_id, blob)

    async def upload_model_logs(self, app_id, model_type, mdl_id, blob):
        container = "models-%s" % app_id
        mdl_id = "logs-%s-%s" % (model_type, mdl_id)

        async with ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client: 
            await client.upload_blob(mdl_id, blob, overwrite=True)

    async def upload_model_valid(self, app_id, model_type, mdl_id, blob):
        container = "models-%s" % app_id
        mdl_id = "valid-%s-%s" % (model_type, mdl_id)

        async with ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client: 
            await client.upload_blob(mdl_id, blob)

    async def upload_model_component(self, app_id, model_type, mdl_id, component, blob):
        container = "models-%s" % app_id
        mdl_id = "%s-%s-%s" % (model_type, mdl_id, component)

        async with ContainerClient(container_name=container, account_url=self.AZURE_ACC_URL, credential=self.AZURE_ACC_KEY) as client: 
            await client.upload_blob(mdl_id, blob, overwrite=True)

    async def delete_model(self, app_id, model_type, mdl_id):
        container = "models-%s" % app_id
        mdl_id = "%s-%s" % (model_type, mdl_id)
        await self._delete_blobs(container, [mdl_id])
