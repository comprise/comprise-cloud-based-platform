"""
    COMPRISE Cloud Platform API v1
    Speech data manipulation API handlers
"""
import json
import logging
import datetime
import tornado.escape
import base64
from functools import wraps

from api_handlers.v1_common import V1Handler, assert_app_exists
import api_handlers.v1_common
from utils.oidc_handler import oidc_protected
import db

from typing import List

# TODO: model types should be configurable
MODEL_TYPES = ["ASR", "NLU"]

def assert_model_type_exists():
    def decorator(f):
        @wraps(f)
        async def wrapper(*args, **kwds):
            self = args[0]
            model_type = kwds["model_type"]

            if model_type not in MODEL_TYPES:
                # model type not found
                self.set_status(404)
                # error messages shall be allowed for all origins
                self.set_header('Access-Control-Allow-Origin', "*")
                logging.info("Model type %s not found" % model_type)
                self.finish()
                return None

            return await f(*args, **kwds)

        return wrapper

    return decorator

class ApplicationsModelTypesHandler(V1Handler):

    @oidc_protected(required_scopes=["general"], required_roles=["developer"], fail_on_empty=False)
    @assert_app_exists()
    async def get(self, app_id):
        '''List applications app_id model types
        ---
        summary: List of model types
        description: Retrieve a list of model types
        tags: 
        - application_models
        security:
          - open_id: ["general"]
          - api_key: []
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        responses:
            401: 
               "$ref": "#/components/responses/NotAuthorized"
            200:
                description: list of model types
                content:
                  application/json: 
                    schema:
                      type: object
                      properties:
                        segments:
                          type: array
                          items:
                            type: string
        '''
        dbc = db.DB()
        # list all applications available for client
        if not await self.has_resource_access("application/model.get", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        # output as object        
        lst = {"types": MODEL_TYPES}
        self.write_json(lst)
        self.finish()


class ApplicationsModelsCollectionHandler(V1Handler):

    @oidc_protected(required_scopes=["general"], required_roles=["developer"], fail_on_empty = False)
    @assert_app_exists()
    @assert_model_type_exists()
    async def get(self, app_id, model_type):
        '''List applications app_id models of particular type
        ---
        summary: Get list of models
        description: Get a list of models of particular type trained by application
        tags: 
        - application_models
        security:
          - open_id: ["general"]
          - api_key: []
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        - name: model_type
          in: path
          description: model type
          required: true
          schema:
            type: string
        - name: filter_recipe
          in: query
          description: filter list by recipe
          required: false
          schema:
            type: string
        - name: filter_status
          in: query
          description: filter list by status
          required: false
          schema:
            type: string
        - name: skip
          in: query
          description: number of items to skip
          required: false
          schema:
            type: integer
            format: int32
        - name: limit
          in: query
          description: max records to return
          required: false
          schema:
            type: integer
            format: int32
        responses:
            404:
               description: application or model type not exists
            401: 
               "$ref": "#/components/responses/NotAuthorized"
            200:
                description: list of models of type model_type
                content:
                  application/json: 
                    schema:
                      type: object
                      properties:
                        models:
                          type: array
                          items:
                            type: object
                            properties:
                              id:
                                type: string
                              created:
                                type: string
                              trained:
                                type: string
                              latest:
                                type: boolean
                              status:
                                type: string
                              recipe:
                                type: string
                              is_mt:
                                type: boolean
                              additonal_corpora:
                                type: array                     
                                description: additional corpora from other applications
                                items:
                                  type: object
                                  properties:
                                    app_id:
                                      type: string
        ''' 

        # list all applications available for client
        if not await self.has_resource_access("application/model.get", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        # TODO: filter, pagination?
        dbc = db.DB()
        skip = int(self.get_argument("skip", 0))
        limit = int(self.get_argument("limit", 100))
        filter_recipe = str(self.get_argument("filter_recipe", ""))
        filter_status = str(self.get_argument("filter_status", ""))

        # list all models available for application
        lst = [i async for i in dbc.get_model_list(model_type, app_id, skip = skip, limit = limit, recipe = filter_recipe, status = filter_status)]

        # output as object        
        lst = {"models": lst}
        self.write_json(lst)
        self.finish()

    @oidc_protected(required_scopes=["general"], required_roles=["developer"])
    @assert_app_exists()
    @assert_model_type_exists()
    async def post(self, app_id, model_type):
        '''Train new model
        ---
        description: Train new model using speech or text corpus of application 
        summary: Train new model
        tags: 
        - application_models
        security:
          - open_id: ["general"]
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        - name: model_type
          in: path
          description: model type
          required: true
          schema:
            type: string
        requestBody:
          description: training job details
          content:
            application/json:
               schema:
                 type: object
                 properties:
                   is_mt:
                     type: boolean
                     default: false
                   recipe:
                     type: string
                   additional_corpora:
                     type: array                     
                     description: use additional corpora from other applications
                     items:
                       type: object
                       properties:
                         app_id:
                           type: string
                         share_key:
                           type: string
        responses:
            200:
                description: request submitted
            400:
                description: invalid request body
            401:
                "$ref": "#/components/responses/NotAuthorized"
            404:
                description: application not found or invalid model type
        '''

        if not await self.has_resource_access("application/model.train", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        try:
            data = tornado.escape.json_decode(self.request.body)
        except json.JSONDecodeError as e:
            logging.warn("POST 'applications/%s/models/%s'. Failed to parse JSON %s" % (app_id, model_type, str(e)))
            self.set_status(400)
            self.write(str(e))
            return
        
        mdl = {}
        mdl["is_mt"] = data.get("is_mt", False)
        mdl["recipe"] = data.get("recipe", "default")
        mdl["owner_id"] = self.client_id
        mdl["app_id"] = app_id
        mdl["type"] = model_type
        mdl["created"] = datetime.datetime.utcnow()
        mdl["trained"] = ""
        mdl["status"] = "submitted"
        mdl["latest"] = False
        mdl["additional_corpora"] = []

        dbc = db.DB()
        # TODO: validate data
        # check access to additional corpora 
        try:
            for app in data.get("additional_corpora", []):
                # try to get application with particular id and share_key
                await dbc.get_application(app["app_id"], share_key = app["share_key"]) # check if access key is correct
                mdl["additional_corpora"].append({"app_id": app["app_id"]})
        except KeyError as e:
            logging.warn("POST 'applications/%s/models/%s'. Invalid additional corpora provided %s" % (app_id, model_type, str(e)))
            self.set_status(404)
            self.write(str(e))
            return

        mdl_id = await dbc.create_model(mdl)
        app = await dbc.get_application(app_id)
        if mdl_id:
            # create app containers for corpora
            logging.debug("Creating containers for app %s" % app_id)
            await self.application.storage_service.create_containers(app_id)
            # submit job
            job = {"model": mdl, "app": app}
            key = "%s.%s" % (mdl["type"], mdl["recipe"])
            self.application.job_publisher.publish_job(job, routing_key=key, on_confirm=self._job_confirmed)
        else:
            logging.warn("Unknown server error, could not create training job")
            self.set_status(500)

    async def _job_confirmed(self, job, confirmation_type):
        dbc = db.DB()
        if confirmation_type == "ack":
           await dbc.change_model_status(job["model"]["_id"], "schedule.ok")
        else:
           await dbc.change_model_status(job["model"]["_id"], "schedule.failed")


class ApplicationsModelsHandler(V1Handler):

    @oidc_protected(required_scopes=["general"], required_roles=["developer"], fail_on_empty = False)
    @assert_app_exists()
    @assert_model_type_exists()
    async def get(self, app_id, model_type, mdl_id):
        '''Get model <mdl_id>
        ---
        summary: Download model
        description: Get trained model from application
        tags: 
        - application_models
        security:
          - open_id: ["general"]
          - api_key: []
        parameters:
        - name: app_id
          in: path
          description: ID of the application
          required: true
          schema:
            type: string
        - name: model_type
          in: path
          description: model type
          required: true
          schema:
            type: string
        - name: mdl_id
          in: path
          description: ID of the model
          required: true
          schema:
            type: string
        responses:
            404:
              description: "Application or model not found"
            401: 
               "$ref": "#/components/responses/NotAuthorized"
            302:
              description: "Redirect to model binary file"
              headers:
                Location:
                  description: "URL of the model"
                  schema: {type: string}
        '''
        dbc = db.DB()

        if not await self.has_resource_access("application/model.get", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        try:
            url = self.application.storage_service.get_model_download_url(app_id, model_type, mdl_id)
            self.redirect(url)
        except KeyError:
            # segment not found
            self.set_status(404)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.finish()

    @oidc_protected(required_scopes=["general"], required_roles=["developer"])
    @assert_app_exists()
    @assert_model_type_exists()
    async def delete(self, app_id, model_type, mdl_id):
        ''' Delete model
        ---
        summary: Delete model
        description: Delete trained model
        tags: 
        - application_models
        security:
          - open_id: ["general"]
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        - name: model_type
          in: path
          description: model type
          required: true
          schema:
            type: string
        - name: mdl_id
          in: path
          description: ID of the model
          required: true
          schema:
            type: string
        responses:
            200:
              description: "Model deleted successfully"
            401: 
              "$ref": "#/components/responses/NotAuthorized"
            404:
              description: "Application or model not found"
        '''

        if not await self.has_resource_access("application/model.delete", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        try:
            dbc = db.DB()
            app = await dbc.get_application(app_id) # check if app exists
            # delete model from database
            await dbc.delete_model(mdl_id)
            # delete model binary
            await self.application.storage_service.delete_model(app_id, model_type, mdl_id)
        except KeyError:
            # application not found
            self.set_status(404)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.finish()
            return


class ApplicationsModelComponentsHandler(V1Handler):

    @oidc_protected(required_scopes=["general"], required_roles=["developer"], fail_on_empty = False)
    @assert_app_exists()
    @assert_model_type_exists()
    async def get(self, app_id, model_type, mdl_id, component):
        '''Get specific component of model <mdl_id>
        ---
        summary: Get model component
        description: Get model component from application
        tags: 
        - application_models
        security:
          - open_id: ["general"]
          - api_key: []
        parameters:
        - name: app_id
          in: path
          description: ID of the application
          required: true
          schema:
            type: string
        - name: model_type
          in: path
          description: model type
          required: true
          schema:
            type: string
        - name: mdl_id
          in: path
          description: ID of the model
          required: true
          schema:
            type: string
        - name: component
          in: path
          description: |
             component of the model
             
             Models trained with "Unified Training Recipe" will have the following components:
             - "lm" -  language model prepared for usage in speech-to-text. HCLG graph for lookahead decoding. Trained on texts collected by the app.
             - "lm_arpa" - language model in ARPA format (gzipped).
             - "tdnn" - TDNN acoustic model.
             - "tdnn_online" - TDNN acoustic model configured for online recognition.
             - "graph_lookahead" - language model prepared for usage in speech-to-text. HCLG graph for lookahead decoding. Trained on audio transcripts.
             - "gram" - language model prepared for usage in speech-to-text. HCLG graph for lookahead decoding. Trained using special JSGF grammar.
          required: true
          schema:
            type: string
        responses:
            404:
              description: "Application, model or component not found"
            401: 
               "$ref": "#/components/responses/NotAuthorized"
            302:
              description: "Redirect to model binary file"
              headers:
                Location:
                  description: "URL of the model"
                  schema: {type: string}
        '''
        dbc = db.DB()

        if not await self.has_resource_access("application/model.get", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        try:
            if component == "logs":
                # logs requested
                url = self.application.storage_service.get_model_logs_download_url(app_id, model_type, mdl_id)
            elif component == "validation":
                # logs requested
                url = self.application.storage_service.get_model_valid_download_url(app_id, model_type, mdl_id)
            else:
                # other component
                url = self.application.storage_service.get_model_component_download_url(app_id, model_type, mdl_id, component)
            self.redirect(url)
        except KeyError:
            # segment not found
            self.set_status(404)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.finish()

    @oidc_protected(required_scopes=["general"], required_roles=["developer"])
    @assert_app_exists()
    @assert_model_type_exists()
    async def post(self, app_id, model_type, mdl_id, component):
        '''Retrain specific component of model <mdl_id>
        ---
        summary: Retrain model component
        description: Retrain model component from application
        tags: 
        - application_models
        security:
          - open_id: ["general"]
        parameters:
        - name: app_id
          in: path
          description: ID of the application
          required: true
          schema:
            type: string
        - name: model_type
          in: path
          description: model type
          required: true
          schema:
            type: string
        - name: mdl_id
          in: path
          description: ID of the model
          required: true
          schema:
            type: string
        - name: component
          in: path
          description:  |
             component of the model
             
             Models trained with "Unified Training Recipe" have following retrainable components:
             - "lm" -  this will train language model on texts collected by the app and prepare this model for usage in speech-to-text (will create HCLG.fst for lookahead decoding). 
             - "weaksup" - this will perform weakly-supervised STT training with Err2Unk. "tdnn", "tdnn_online", "lm_arpa" and "graph_lookahead" components will be updated as the result.
             - "gram" - this will train an adapted language model using the JSGF grammar provided in the request body. 
          required: true
          schema:
            type: string
        requestBody:
          description: | 
            optional file containing data specific to the component being retrained

            For "gram" component the attachement should be a file containing JSGF grammar.
          required: false
          content:
            multipart/form-data:
              schema:
                type: object
                properties:
                  attachement:
                    type: string
                    format: binary
        responses:
            200:
                description: request submitted
            400:
                description: invalid request 
            401:
                "$ref": "#/components/responses/NotAuthorized"
            404:
                description: "Application, model or component not found"
        '''
        dbc = db.DB()

        if not await self.has_resource_access("application/model.train", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        try:
            mdl = await dbc.get_model(app_id, mdl_id) # load model
            app = await dbc.get_application(app_id) # load application

            # someday there might be more params inside dict below
            mdl['component'] = {
                "name": component
            }            
            if component == "logs":
                # invalid request
               self.set_status(400)
               # error messages shall be allowed for all origins
               self.set_header('Access-Control-Allow-Origin', "*")
               self.set_header("Content-Type", "text/plain")
               self.write("Invalid request")
               self.finish()
            else:
                # submit job
                job = {"model": mdl, "app": app}
                key = "%s.%s" % (mdl["type"], mdl["recipe"])

                if 'attachement' in self.request.files:
                    file1 = self.request.files['attachement'][0]
                    job["attachement"] = base64.b64encode(file1['body']).decode("utf-8")

                self.application.job_publisher.publish_job(job, routing_key=key, on_confirm=self._job_confirmed)
        except json.JSONDecodeError as e:
            logging.warn("POST 'applications/%s/models/%s/%s/%s'. Failed to parse JSON %s" % (app_id, model_type, mdl_id, component, str(e)))
            self.set_status(400)
            self.write(str(e))
            self.finish()
        except KeyError:
            # segment not found
            self.set_status(404)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.finish()

    async def _job_confirmed(self, job, confirmation_type):
        dbc = db.DB()
        if confirmation_type == "ack":
           await dbc.change_model_status(job["model"]["_id"], "schedule.ok")
        else:
           await dbc.change_model_status(job["model"]["_id"], "schedule.failed")
