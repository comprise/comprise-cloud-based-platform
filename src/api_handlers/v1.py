"""
    COMPRISE Cloud Platform API v1
"""
import json
import logging
import secrets
from apispec import APISpec
from apispec_webframeworks.tornado import TornadoPlugin

import tornado.web

from utils.oidc_handler import oidc_protected
from api_handlers.v1_speech_data import ApplicationsSpeechCollectionHandler, ApplicationsSpeechHandler
from api_handlers.v1_text_data import ApplicationsTextCollectionHandler, ApplicationsTextHandler
from api_handlers.v1_models import (
    ApplicationsModelsCollectionHandler, ApplicationsModelsHandler, ApplicationsModelTypesHandler, ApplicationsModelComponentsHandler
)
from api_handlers.v1_common import V1Handler
import api_handlers.v1_common
import db

from typing import List


def register(handlers: List) -> None:
    # list of url paths and handlers
    prefix = api_handlers.v1_common.API_VERSION_PREFIX
    urlspecs = [(r"/%s/applications" % prefix, ApplicationsCollectionHandler, {}),
                (r"/%s/applications/(?P<app_id>[a-z0-9]*)" % prefix, ApplicationsHandler, {}),
                (r"/%s/applications/(?P<app_id>[a-z0-9]*)/speech" % prefix, ApplicationsSpeechCollectionHandler, {}),
                (r"/%s/applications/(?P<app_id>[a-z0-9]*)/speech/(?P<utt_id>[a-z0-9-]*)" % prefix, ApplicationsSpeechHandler, {}),
                (r"/%s/applications/(?P<app_id>[a-z0-9]*)/text" % prefix, ApplicationsTextCollectionHandler, {}),
                (r"/%s/applications/(?P<app_id>[a-z0-9]*)/text/(?P<utt_id>[a-z0-9-]*)" % prefix, ApplicationsTextHandler, {}),
                (r"/%s/applications/(?P<app_id>[a-z0-9]*)/models" % prefix, ApplicationsModelTypesHandler, {}),
                (r"/%s/applications/(?P<app_id>[a-z0-9]*)/models/(?P<model_type>[A-z0-9-]*)" % prefix, ApplicationsModelsCollectionHandler, {}),
                (r"/%s/applications/(?P<app_id>[a-z0-9]*)/models/(?P<model_type>[A-z0-9-]*)/(?P<mdl_id>[a-z0-9-]*)" % prefix, ApplicationsModelsHandler, {}),
                (r"/%s/applications/(?P<app_id>[a-z0-9]*)/models/(?P<model_type>[A-z0-9-]*)/(?P<mdl_id>[a-z0-9-]*)/(?P<component>[a-z0-9_]*)" 
                                                                                                           % prefix, ApplicationsModelComponentsHandler, {})]
    handlers += urlspecs

    # register OpenAPI handler for serving Swagger spec
    handlers.append((r"/%s/.well-known/service-desc" % prefix, OpenAPIHandler, {"api_handlers": urlspecs})) 


class OpenAPIHandler(tornado.web.RequestHandler):
    '''Serve OpenAPI v3 (swagger) spec which describes this API
    '''

    def initialize(self, api_handlers):
        # initialize API spec

        # server spec
        servers = [{"url": self.application.etc_conf.get("public_address", "localhost"), 
                    "description": self.application.etc_conf.get("server_description", "COMPRISE API service")}]

        # apispec initialization
        spec = APISpec(
            title="COMPRISE Cloud Platform API",
            version="v1alpha",
            openapi_version="3.0.2",
            info=dict(description="COMPRISE Cloud Platform API v1alpha"),
            servers=servers,
            plugins=[TornadoPlugin()],
        )

        api_key_scheme = {"type": "apiKey", "in": "query", "name": "api_key"}
        oidc_scheme = {"type": "openIdConnect", "openIdConnectUrl": "/.well-known/openid-configuration"}
        jwt_scheme = {"type": "http", "scheme": "bearer", "bearerFormat": "JWT"}
        oauth2_scheme = {"type": "oauth2",
                         "flows": {
                           "authorizationCode": {
                             "authorizationUrl": self.application.jwt_validator.get_discovery_conf()["authorization_endpoint"],
                             "tokenUrl": self.application.jwt_validator.get_discovery_conf()["token_endpoint"],
                             "scopes": {
                               "openid": "OpenID login",
				self.application.jwt_validator.get_scope_prefix()+"general": "COMPRISE",
                             }
                           },
                           "implicit": {
                             "authorizationUrl": self.application.jwt_validator.get_discovery_conf()["authorization_endpoint"],
                             "scopes": {
				self.application.jwt_validator.get_scope_prefix()+"general": "COMPRISE",
                               "openid": "OpenID login"
                             }
                           }
                         }
                        }

        spec.components.security_scheme("api_key", api_key_scheme)
        # for debug, remove later
        #spec.components.security_scheme("open_id", jwt_scheme)
        # TODO: uncomment when fully supported
        #spec.components.security_scheme("open_id", oidc_scheme)
        # OpenID Connect auth
        spec.components.security_scheme("open_id", oauth2_scheme)

        spec.components.response("NotAuthorized", {"description": "User not authorized to access specified resource",  
                                                    "headers": {
                                                      "WWW-Authenticate": {
                                                        "description": "error details",
                                                        "schema": {"type": "string"}
                                                      }
                                                    }
                                                   })

        # register handlers in APISpec
        for url_tuple in api_handlers:
            spec.path(urlspec=url_tuple)

        self.spec = spec

    def options(self, *args, **kwargs) -> None:
        self.set_header('Access-Control-Allow-Origin', '*')
        self.finish()

    async def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.write(self.spec.to_yaml())
        # quick test
        dbc = db.DB()
        lst = [i async for i in dbc.get_application_list()]
        self.finish()


class ApplicationsCollectionHandler(V1Handler):

    def _gen_api_key(self) -> str:
        return secrets.token_urlsafe(32)

    @oidc_protected(required_scopes=["general"], required_roles=["developer"])
    async def get(self):
        '''List applications
        ---
        summary: List of applicatons
        description: Get a list of applications owned by developer
        tags: 
        - application
        security:
          - open_id: ["general"]
        parameters:
        - name: search
          in: query
          description: returns only applications that have specified string in name or description
          required: false
          schema:
            type: string
        - name: skip
          in: query
          description: number of items to skip
          required: false
          schema:
            type: integer
            format: int32
        - name: limit
          in: query
          description: max records to return
          required: false
          schema:
            type: integer
            format: int32
        responses:
            401: 
               "$ref": "#/components/responses/NotAuthorized"
            200:
                description: list of application names and ids
                content:
                  application/json: 
                    schema:
                      type: object
                      properties:
                        applications:
                          type: array
                          items:
                            type: object
                            properties:
                              id:
                                type: string
                              name:
                                type: string
        '''
        dbc = db.DB()
        skip = int(self.get_argument("skip", 0))
        limit = int(self.get_argument("limit", 100))
        search = str(self.get_argument("search", ""))
        
        # list all applications available for client
        if "admin" in self.client_roles:
            lst = [i async for i in dbc.get_application_list(skip=skip,limit=limit,search=search)]
        else:
            lst = [i async for i in dbc.get_application_list(self.client_id, skip=skip, limit=limit, search=search)]

        # output as object        
        lst = {"applications": lst}
        self.write_json(lst)
        self.finish()

    @oidc_protected(required_scopes=["general"], required_roles=["developer"])
    async def post(self):
        '''Register new application
        ---
        summary: Register new application
        description: Register new application
        tags: 
        - application
        security:
          - open_id: ["general"]
        requestBody:
          description: application data (other fields will be ignored)
          content:
            application/json:
               schema:
                 type: object
                 properties:
                   name:
                     type: string
                   app_key:
                     type: string
                     example: "12384abcdef or empty to use auto-generated"
                     description: "API key for mobile apps"
                   share_key:
                     type: string
                     example: "12384abcdef or empty to use auto-generated"
                     description: "key for sharing corpus between applications"
                   annotator_key:
                     type: string
                     example: "12384abcdef or empty to use auto-generated"
                     description: "access key for annotators"
                   language: 
                      type: string
                      description: language of the application (ISO 639-1) 
                   description:
                      type: string
                 required:
                   - name
                   - language
        responses:
            200:
              description: "Application JSON document"
            302:
              description: "Redirect to created application"
              headers:
                Location:
                  description: "URL of new application"
                  schema: {type: string}
            400:
              description: "Invalid application JSON"
            401: 
              "$ref": "#/components/responses/NotAuthorized"
        '''
        try:
            data = tornado.escape.json_decode(self.request.body)
        except json.JSONDecodeError:
            logging.warn("POST 'applications/'. Failed to parse JSON %s" % str(e))
            self.set_status(400)
            self.write(str(e))
            return

        post_fields = ["name", "language", "description", "app_key", "annotator_key", "share_key"]
        app = {}
        for f in post_fields:
            app[f] = data.get(f, "")
        
        app["owner_id"] = self.client_id
        if not app["app_key"]:
            app["app_key"] = self._gen_api_key()
        if not app["annotator_key"]:
            app["annotator_key"] = self._gen_api_key()
        if not app["share_key"]:
            app["share_key"] = self._gen_api_key()

        # TODO: validate data

        dbc = db.DB()
        app_id = await dbc.create_application(app)
        if app_id:
            # create app containers for corpora
            logging.debug("Creating containers for app %s" % app_id)
            await self.application.storage_service.create_containers(app_id)
        self.redirect("/%s/applications/%s" % (api_handlers.v1_common.API_VERSION_PREFIX, app_id))


class ApplicationsHandler(V1Handler):

    def _gen_api_key(self) -> str:
        return secrets.token_urlsafe(32)

    @oidc_protected(required_scopes=["general"], required_roles=["developer"], fail_on_empty = False)
    async def get(self, app_id):
        '''Get application data
        ---
        summary: Get application data
        description: |
          Get data for specified application.
        
          ### NB: Using this method you can get URLs for direct upload of speech and text data. 
          ### For example, if application wants to send speech data it needs only to do HTTP PUT to speech_upload_url.

          ### Example CURL command:
          `curl -H "x-ms-blob-type: BlockBlob" -X PUT --data-binary "@audio.wav" "<speech_upload_url>"`
        
          ### NB: Upload URL allows to upload single file only! Application should get a new upload URL each time it wants to upload a new file.
        tags: 
        - application
        security:
          - open_id: ["general"]
          - api_key: []
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        responses:  
            401: 
              "$ref": "#/components/responses/NotAuthorized"
            200:
                description: application data
                content:
                  application/json:
                    schema:
                      type: object
                      properties:
                        id:
                          type: string
                        name:
                          type: string
                        owner_id:
                          type: string
                        app_key:
                          type: string
                          example: "12384abcdef or empty to use auto-generated"
                          description: "API key for mobile apps"
                        share_key:
                          type: string
                          example: "12384abcdef or empty to use auto-generated"
                          description: "key for sharing corpus between applications"
                        annotator_key:
                          type: string
                          example: "12384abcdef or empty to use auto-generated"
                          description: "access key for annotators"
                        speech_upload_url:
                          type: string
                          description: see note in the method description
                        text_upload_url:
                          type: string
                          description: see note in the method description
                        language: 
                          type: string
                          description: language of the application (ISO 639-1) 
                        description:
                            type: string        
        '''
        dbc = db.DB()
        # client requests info on particular application
        if await self.has_resource_access("application.get", app_id):
            # client authorized via oidc or AppKey
            try:
                app = await dbc.get_application(app_id)
                # TODO: get application upload urls
                logging.debug("getting upload urls for app %s" % app_id)
                app["speech_upload_url"] = self.application.storage_service.get_speech_upload_url(app_id)
                app["text_upload_url"] = self.application.storage_service.get_text_upload_url(app_id)
                self.write_json(app)
            except KeyError:
                # application not found
                self.set_status(404)
                # error messages shall be allowed for all origins
                self.set_header('Access-Control-Allow-Origin', "*")
            self.finish()
            return
        
        # client not authorized
        self.set_status(401)
        # error messages shall be allowed for all origins
        self.set_header('Access-Control-Allow-Origin', "*")
        self.set_header("Content-Type", "text/plain")
        self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
        self.write("Insufficient permissions")
        self.finish()

    @oidc_protected(required_scopes=["general"], required_roles=["developer"])
    async def put(self, app_id):
        '''Edit application data
        ---
        summary: Edit application data
        description: Edit application data
        tags: 
        - application
        security:
          - open_id: ["general"]
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        requestBody:
          description: application data (other fields will be ignored)
          content:
            application/json:
               schema:
                 type: object
                 required:
                   - name
                   - language
                 properties:
                   name:
                     type: string
                   app_key:
                     type: string
                     example: "12384abcdef or empty to use auto-generated"
                     description: "API key for mobile apps"
                   share_key:
                     type: string
                     example: "12384abcdef or empty to use auto-generated"
                     description: "key for sharing corpus between applications"
                   annotator_key:
                     type: string
                     example: "12384abcdef or empty to use auto-generated"
                     description: "access key for annotators"
                   language: 
                      type: string
                      description: language of the application (ISO 639-1) 
                   description:
                      type: string
        responses:
            200:
              description: "Application modified successfully"
            400:
              description: "Invalid application JSON"
            401: 
              "$ref": "#/components/responses/NotAuthorized"
        '''

        if not await self.has_resource_access("application.put", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        try:
            data = tornado.escape.json_decode(self.request.body)
        except json.JSONDecodeError as e:
            logging.warn("PUT 'applications/%s'. Failed to parse JSON %s" % (app_id, str(e)))
            self.set_status(400)
            self.write(str(e))
            return

        dbc = db.DB()

        # TODO: validate data
        data["owner_id"] = self.client_id
        if "speech_upload_url" in data:
            del data["speech_upload_url"]
        if "text_upload_url" in data:
            del data["text_upload_url"]

        # keys
        if not data["app_key"]:
            data["app_key"] = self._gen_api_key()
        if not data["annotator_key"]:
            data["annotator_key"] = self._gen_api_key()
        if not data.get("share_key", None):
            data["share_key"] = self._gen_api_key()

        success = await dbc.store_application(app_id, data)

        if success:
            self.set_status(200)
        else:
            self.set_status(400)

    @oidc_protected(required_scopes=["general"], required_roles=["developer"])
    async def delete(self, app_id):
        '''Un-register application and delete all data
        ---
        summary: Delete application
        description: Un-register application and delete all collected data and models
        tags: 
        - application
        security:
          - open_id: ["general"]
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        responses:
            200:
              description: "Application deleted successfully"
            401: 
              "$ref": "#/components/responses/NotAuthorized"
            404:
              description: "Application not found"
        '''

        if not await self.has_resource_access("application.delete", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        dbc = db.DB()

        try:
            # TODO: delete all associated resources
            await dbc.delete_application(app_id)
            self.set_status(200)
        except KeyError:      
            self.set_status(404)



