"""
    COMPRISE Cloud Platform API v1
"""
import json
import logging
import datetime
from bson.objectid import ObjectId
import tornado.web
from functools import wraps

import db

API_VERSION_PREFIX = "v1alpha"

def assert_app_exists():
    def decorator(f):
        @wraps(f)
        async def wrapper(*args, **kwds):
            self = args[0]
            app_id = kwds["app_id"]

            try:
                dbc = db.DB()
                app = await dbc.get_application(app_id) # check if app exists
            except KeyError:
                # application not found
                self.set_status(404)
                # error messages shall be allowed for all origins
                self.set_header('Access-Control-Allow-Origin', "*")
                logging.info("Application %s not found" % app_id)
                self.finish()
                return None

            return await f(*args, **kwds)

        return wrapper

    return decorator

class V1Handler(tornado.web.RequestHandler):
    '''Superclass for all API v1 handlers
    
       Provides basic functions which are common to all API v1 handlers
    '''
    auth_disabled = False

    class MongoEncoder(json.JSONEncoder):
        def default(self, obj):

            if isinstance(obj, ObjectId):
                return str(obj)
            if isinstance(obj, datetime.datetime):
                return obj.isoformat()

            return json.JSONEncoder.default(self, obj)

    def prepare(self) -> None:
        # this is a public API, allow access from all origins
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', 'accept, content-type, authorization')

    def options(self, *args, **kwargs) -> None:
        self.set_header('Access-Control-Allow-Methods', ", ".join(self.SUPPORTED_METHODS))
        self.finish()

    def write_json(self, obj: dict) -> None:
        self.set_header("Content-Type", "application/json; charset=UTF-8")
        self.write(json.dumps(obj, cls=self.MongoEncoder, indent=2))

    async def has_resource_access(self, resource_kind, resource_id):
        # TODO: this basically is hard-coded part of authorization, consider rewrite
        if "admin" in self.client_roles:
            # administrator have no restrictions
            return True

        if (resource_kind == "application.get"):
            dbc = db.DB()
            key = self.get_argument("api_key", "")
            app = await dbc.get_application(resource_id)
            if app["app_key"] == key:
                return True
            if self.client_id == app["owner_id"]:
                return True

        if (resource_kind == "application.delete" or
            resource_kind == "application.put"):
            dbc = db.DB()
            app = await dbc.get_application(resource_id)
            if self.client_id == app["owner_id"]:
                return True

        if (resource_kind == "application/speech.get" or
            resource_kind == "application/speech.patch" or
            resource_kind == "application/speech.delete"):
            dbc = db.DB()
            key = self.get_argument("api_key", "")
            app = await dbc.get_application(resource_id)
            if app["annotator_key"] == key:
                return True
            if self.client_id == app["owner_id"]:
                return True

        if (resource_kind == "application/model.get" or
            resource_kind == "application/model.train" or
            resource_kind == "application/model.delete"):
            dbc = db.DB()
            key = self.get_argument("api_key", "")
            app = await dbc.get_application(resource_id)
            if app["app_key"] == key:
                return True
            if self.client_id == app["owner_id"]:
                return True

        if (resource_kind == "application/text.get" or
            resource_kind == "application/text.patch" or
            resource_kind == "application/text.delete"):
            dbc = db.DB()
            key = self.get_argument("api_key", "")
            app = await dbc.get_application(resource_id)
            if app["annotator_key"] == key:
                return True
            if self.client_id == app["owner_id"]:
                return True

        return False
