"""
    COMPRISE Cloud Platform API v1
    Speech data manipulation API handlers
"""
import json
import logging
import tornado.escape

from api_handlers.v1_common import V1Handler, assert_app_exists
from utils.oidc_handler import oidc_protected
import db

from typing import List


class ApplicationsSpeechCollectionHandler(V1Handler):

    @oidc_protected(required_scopes=["general"], required_roles=["annotator"], fail_on_empty = False)
    @assert_app_exists()
    async def get(self, app_id):
        '''List applications app_id speech segments
        ---
        description: Get a list of speech segments uploaded by application
        summary: List of speech segments
        tags: 
        - application_speech_data
        security:
          - open_id: ["general"]
          - api_key: []
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        - name: search
          in: query
          description: name starts with 
          required: false
          schema:
            type: string
        - name: skip
          in: query
          description: number of items to skip
          required: false
          schema:
            type: integer
            format: int32
        - name: limit
          in: query
          description: max records to return
          required: false
          schema:
            type: integer
            format: int32
        responses:
            401: 
               "$ref": "#/components/responses/NotAuthorized"
            200:
                description: list of speech segments ids
                content:
                  application/json: 
                    schema:
                      type: object
                      properties:
                        segments:
                          type: array
                          items:
                            type: object
                            properties:
                              id:
                                type: string
                              audio_url:
                                type: string
                              annotation_url:
                                type: string
        '''
        dbc = db.DB()
        skip = int(self.get_argument("skip", 0))
        limit = int(self.get_argument("limit", 100))
        search = str(self.get_argument("search", ""))

        # list all applications available for client
        if not await self.has_resource_access("application/speech.get", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        # TODO: filter, pagination?
        lst = [i for i in await self.application.storage_service.get_speech_segments(app_id, skip=skip, limit=limit, name_starts_with=search)]

        # output as object        
        lst = {"segments": lst}
        self.write_json(lst)
        self.finish()

    @oidc_protected(required_scopes=["general"], required_roles=["developer"], fail_on_empty = False)
    @assert_app_exists()
    async def post(self, app_id):
        '''Upload speech segment
        ---
        description: |
          Upload a new speech segment. 
        
          ### NB: NOT IMPLEMENTED!!! Use speech_upload_url from GET /applications/<id> instead
        summary: Upload speech segment
        tags: 
        - application_speech_data
        security:
          - open_id: ["general"]
          - api_key: []
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        responses:
            501:
                description: method not implemented
        '''

        logging.warn("Call to not-implemented API 'POST applications/<id>/speech'!")
        self.set_status(501)
        return

    @oidc_protected(required_scopes=["general"], required_roles=["developer"])
    @assert_app_exists()
    async def delete(self, app_id):
        ''' Delete multiple uploaded speech segments
        ---
        description: Delete speech segments
        summary: Delete speech segments
        tags: 
        - application_speech_data
        security:
          - open_id: ["general"]
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        requestBody:
          description: list of speech segments ids to delete
          required: true
          content:
            application/json:
               schema:
                 type: array
                 items:
                   type: string
        responses:
            200:
              description: "Speech segments deleted successfully"
            400:
              description: "Invalid request body"
            401: 
              "$ref": "#/components/responses/NotAuthorized"
            404:
              description: "Application not found"
        '''

        if not await self.has_resource_access("application/speech.delete", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        try:
            dbc = db.DB()
            app = await dbc.get_application(app_id) # check if app exists
            segments = tornado.escape.json_decode(self.request.body)
        except KeyError:
            # application not found
            self.set_status(404)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.finish()
            return
        except json.JSONDecodeError:
            logging.warn("DELETE 'applications/%s/speech'. Failed to parse JSON %s" % (app_id, str(e)))
            self.set_status(400)
            self.write(str(e))
            return

        await self.application.storage_service.delete_speech_segments(app_id, segments)


class ApplicationsSpeechHandler(V1Handler):

    @oidc_protected(required_scopes=["general"], required_roles=["annotator"], fail_on_empty = False)
    @assert_app_exists()
    async def get(self, app_id, utt_id):
        '''Get info on speech segment <utt_id>
        ---
        description: Get speech segment <utt_id> from application <app_id> data
        summary: Get speech segment
        tags: 
        - application_speech_data
        security:
          - open_id: ["general"]
          - api_key: []
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        - name: utt_id
          in: path
          description: ID of speech segment
          required: true
          schema:
            type: string
        responses:
            401: 
               "$ref": "#/components/responses/NotAuthorized"
            200:
                description: speech segment description
                content:
                  application/json: 
                    schema:
                      type: object
                      properties:
                        id:
                          type: string
                        audio_url:
                          type: string
                        annotation_url:
                          type: string
        '''
        dbc = db.DB()
        # list all applications available for client
        if not await self.has_resource_access("application/speech.get", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        try:
            seg = await self.application.storage_service.get_speech_segment(app_id, utt_id)
            # output as object        
            self.write_json(seg)
            self.finish()
        except KeyError:
            # segment not found
            self.set_status(404)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.finish()

    @oidc_protected(required_scopes=["general"], required_roles=["developer"], fail_on_empty = False)
    @assert_app_exists()
    async def patch(self, app_id, utt_id):
        '''Submit transcription for speech segment <utt_id>
        ---
        description: Submit transcription for speech segment <utt_id>
        summary: Submit transcription
        tags: 
        - application_speech_data
        security:
          - open_id: ["general"]
          - api_key: []
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        - name: utt_id
          in: path
          description: ID of speech segment
          required: true
          schema:
            type: string
        requestBody:
          description: annotation
          content:
            application/json:
               schema:
                 type: object
                 properties:
                   text:
                     type: string
        responses:
            200:
              description: "Annotation modified successfully"
            400:
              description: "Invalid annotation JSON"
            401: 
              "$ref": "#/components/responses/NotAuthorized"
            404:
              description: "Utterance or application not found"
        '''

        if not await self.has_resource_access("application/speech.patch", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        try:
            data = tornado.escape.json_decode(self.request.body)
        except json.JSONDecodeError as e:
            logging.warn("PATCH 'applications/%s/speech/%s'. Failed to parse JSON %s" % (app_id, utt_id, str(e)))
            self.set_status(400)
            self.write(str(e))
            return

        # TODO: validate data

        try:
            # upload JSON document to storage
            # TODO: original or validated?
            await self.application.storage_service.annotate_speech_segment(app_id, utt_id, self.request.body)
            self.finish()
        except KeyError:
            # segment not found
            self.set_status(404)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.finish()

    @oidc_protected(required_scopes=["general"], required_roles=["developer"], fail_on_empty = False)
    @assert_app_exists()
    async def delete(self, app_id, utt_id):
        ''' Delete speech segment
        ---
        description: Delete speech segment
        summary: Delete speech segment
        tags: 
        - application_speech_data
        security:
          - open_id: ["general"]
        parameters:
        - name: app_id
          in: path
          description: ID of application
          required: true
          schema:
            type: string
        - name: utt_id
          in: path
          description: ID of speech segment
          required: true
          schema:
            type: string
        responses:
            200:
              description: "Speech segment deleted successfully"
            400:
              description: "Invalid request body"
            401: 
              "$ref": "#/components/responses/NotAuthorized"
            404:
              description: "Application not found"
        '''

        if not await self.has_resource_access("application/speech.delete", app_id):
            # client not authorized
            self.set_status(401)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.set_header("Content-Type", "text/plain")
            self.set_header("WWW-Authenticate",
                                    "Bearer error=\"invalid_token\", error_description=\"insufficient permissions\"")
            self.write("Insufficient permissions")
            self.finish()
            return

        try:
            dbc = db.DB()
            app = await dbc.get_application(app_id) # check if app exists
            segments = [utt_id]
        except KeyError:
            # application not found
            self.set_status(404)
            # error messages shall be allowed for all origins
            self.set_header('Access-Control-Allow-Origin', "*")
            self.finish()
            return
        except json.JSONDecodeError:
            logging.warn("DELETE 'applications/%s/speech/%s'. Failed to parse JSON %s" % (app_id, str(e), utt_id))
            self.set_status(400)
            self.write(str(e))
            return

        await self.application.storage_service.delete_speech_segments(app_id, segments)
