import pika
import os
import os.path
import logging
import logging.config
import pika.adapters.tornado_connection
import argparse
import json
import yaml
import asyncio
import base64
from azure.core.exceptions import ResourceNotFoundError

import storage
import db
import global_settings

LOGGER = logging.getLogger(__name__)


class JobConsumer(object):
    """This is an example consumer that will handle unexpected interactions
    with RabbitMQ such as channel and connection closures.

    If RabbitMQ closes the connection, it will reopen it. You should
    look at the output, as there are limited reasons why the connection may
    be closed, which usually are tied to permission related issues or
    socket timeouts.

    If the channel is closed, it will indicate a problem with one of the
    commands that were issued and that should surface in the output as well.

    """
    EXCHANGE_TYPE = 'topic'

    def __init__(self, conf, exchange = 'job', routing_key = 'jobs', consume_single = False):
        """Create a new instance of the consumer class, passing in the AMQP
        URL used to connect to RabbitMQ.

        :param str amqp_url: The AMQP url to connect with

        """
        self._etc_conf = conf
        self._connection = None
        self._channel = None
        self._closing = False
        self._consuming = True
        self._consumer_tag = None
        self._url = self._etc_conf["queue_url"]
        self.EXCHANGE = 'job'
        self.QUEUE = routing_key
        self.ROUTING_KEY = routing_key
        self._consume_single = consume_single

        # load logging config
        if "logging" in self._etc_conf:
            logging.config.dictConfig(self._etc_conf["logging"])

        # initialize storage according to config
        self.storage_service = storage.init_storage(self._etc_conf["storage"])

    def connect(self):
        """This method connects to RabbitMQ, returning the connection handle.
        When the connection is established, the on_connection_open method
        will be invoked by pika.

        :rtype: pika.SelectConnection

        """
        LOGGER.info('Connecting to %s', self._url)
        return pika.adapters.tornado_connection.TornadoConnection(
            pika.URLParameters(self._url),
            on_open_callback=self.on_connection_open,
            on_open_error_callback=self.on_connection_open_error)

    def on_connection_open_error(self, _unused_connection, err):
        """This method is called by pika if the connection to RabbitMQ
        can't be established.

        :param pika.SelectConnection _unused_connection: The connection
        :param Exception err: The error

        """
        LOGGER.error('Connection open failed, reopening in 5 seconds: %s', err)
        self._connection.ioloop.call_later(5, self.reconnect)

    def close_connection(self):
        """This method closes the connection to RabbitMQ."""
        LOGGER.info('Closing connection')
        self._connection.close()

    def add_on_connection_close_callback(self):
        """This method adds an on close callback that will be invoked by pika
        when RabbitMQ closes the connection to the publisher unexpectedly.

        """
        LOGGER.info('Adding connection close callback')
        self._connection.add_on_close_callback(self.on_connection_closed)

    def on_connection_closed(self, connection, reason):
        """This method is invoked by pika when the connection to RabbitMQ is
        closed unexpectedly. Since it is unexpected, we will reconnect to
        RabbitMQ if it disconnects.

        :param pika.connection.Connection connection: The closed connection obj
        :param Exception reason: exception representing reason for loss of
            connection.

        """
        self._channel = None
        if self._closing:
            self._connection.ioloop.stop()
        else:
            LOGGER.warning('Connection closed, reopening in 5 seconds: %s',
                           reason)
            self._connection.ioloop.call_later(5, self.reconnect)

    def on_connection_open(self, unused_connection):
        """This method is called by pika once the connection to RabbitMQ has
        been established. It passes the handle to the connection object in
        case we need it, but in this case, we'll just mark it unused.

        :param pika.SelectConnection _unused_connection: The connection

        """
        LOGGER.info('Connection opened')
        self.add_on_connection_close_callback()
        self.open_channel()

    def reconnect(self):
        """Will be invoked by the IOLoop timer if the connection is
        closed. See the on_connection_closed method.

        """
        if not self._closing:

            # Create a new connection
            self._connection = self.connect()

    def add_on_channel_close_callback(self):
        """This method tells pika to call the on_channel_closed method if
        RabbitMQ unexpectedly closes the channel.

        """
        LOGGER.info('Adding channel close callback')
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reason):
        """Invoked by pika when RabbitMQ unexpectedly closes the channel.
        Channels are usually closed if you attempt to do something that
        violates the protocol, such as re-declare an exchange or queue with
        different parameters. In this case, we'll close the connection
        to shutdown the object.

        :param pika.channel.Channel: The closed channel
        :param Exception reason: why the channel was closed

        """
        LOGGER.warning('Channel %i was closed: %s', channel, reason)

        # close connection (TODO: do we need this?)
        if not isinstance(reason, pika.exceptions.StreamLostError):
           if not isinstance(reason, pika.exceptions.ConnectionClosed):
               if self._closing:
                   self._connection.close()

    def on_channel_open(self, channel):
        """This method is invoked by pika when the channel has been opened.
        The channel object is passed in so we can make use of it.

        Since the channel is now open, we'll declare the exchange to use.

        :param pika.channel.Channel channel: The channel object

        """
        LOGGER.info('Channel opened')
        self._channel = channel
        self.add_on_channel_close_callback()
        self.setup_exchange(self.EXCHANGE)

    def setup_exchange(self, exchange_name):
        """Setup the exchange on RabbitMQ by invoking the Exchange.Declare RPC
        command. When it is complete, the on_exchange_declareok method will
        be invoked by pika.

        :param str|unicode exchange_name: The name of the exchange to declare

        """
        LOGGER.info('Declaring exchange %s', exchange_name)
        self._channel.exchange_declare(
            callback=self.on_exchange_declareok,
            exchange=exchange_name,
            exchange_type=self.EXCHANGE_TYPE,
            durable=True
        )

    def on_exchange_declareok(self, unused_frame):
        """Invoked by pika when RabbitMQ has finished the Exchange.Declare RPC
        command.

        :param pika.Frame.Method unused_frame: Exchange.DeclareOk response frame

        """
        LOGGER.info('Exchange declared')
        self.setup_queue(self.QUEUE)

    def setup_queue(self, queue_name):
        """Setup the queue on RabbitMQ by invoking the Queue.Declare RPC
        command. When it is complete, the on_queue_declareok method will
        be invoked by pika.

        :param str|unicode queue_name: The name of the queue to declare.

        """
        LOGGER.info('Declaring queue %s', queue_name)
        self._channel.queue_declare(
            queue=queue_name, durable=True, callback=self.on_queue_declareok)

    def on_queue_declareok(self, method_frame):
        """Method invoked by pika when the Queue.Declare RPC call made in
        setup_queue has completed. In this method we will bind the queue
        and exchange together with the routing key by issuing the Queue.Bind
        RPC command. When this command is complete, the on_bindok method will
        be invoked by pika.

        :param pika.frame.Method method_frame: The Queue.DeclareOk frame

        """
        LOGGER.info('Binding %s to %s with %s',
                    self.EXCHANGE, self.QUEUE, self.ROUTING_KEY)
        self._channel.queue_bind(
            queue=self.QUEUE,
            exchange=self.EXCHANGE,
            routing_key=self.ROUTING_KEY,
            callback=self.on_bindok)

    def add_on_cancel_callback(self):
        """Add a callback that will be invoked if RabbitMQ cancels the consumer
        for some reason. If RabbitMQ does cancel the consumer,
        on_consumer_cancelled will be invoked by pika.

        """
        LOGGER.info('Adding consumer cancellation callback')
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_consumer_cancelled(self, method_frame):
        """Invoked by pika when RabbitMQ sends a Basic.Cancel for a consumer
        receiving messages.

        :param pika.frame.Method method_frame: The Basic.Cancel frame

        """
        LOGGER.info('Consumer was cancelled remotely, shutting down: %r',
                    method_frame)
        if self._channel:
            self._channel.close()

    def acknowledge_message(self, delivery_tag):
        """Acknowledge the message delivery from RabbitMQ by sending a
        Basic.Ack RPC method for the delivery tag.

        :param int delivery_tag: The delivery tag from the Basic.Deliver frame

        """
        LOGGER.info('Acknowledging message %s', delivery_tag)
        self._channel.basic_ack(delivery_tag)

    def on_message(self, unused_channel, basic_deliver, properties, body):
        """Invoked by pika when a message is delivered from RabbitMQ. The
        channel is passed for your convenience. The basic_deliver object that
        is passed in carries the exchange, routing key, delivery tag and
        a redelivered flag for the message. The properties passed in is an
        instance of BasicProperties with the message properties and the body
        is the message that was sent.

        :param pika.channel.Channel unused_channel: The channel object
        :param pika.Spec.Basic.Deliver: basic_deliver method
        :param pika.Spec.BasicProperties: properties
        :param bytes body: The message body

        """
        LOGGER.info('Received message # %s from %s: %s',
                    basic_deliver.delivery_tag, properties.app_id, body)
        if self._consuming == False:
            LOGGER.info('Job already in processing. Rejecting message # %s from %s: %s',
                    basic_deliver.delivery_tag, properties.app_id, body)
            self._channel.basic_reject(basic_deliver.delivery_tag, True)
            return
        # pause consumer (allow only single job to be started)
        self.pause_consuming()
        # parse job
        job = json.loads(body)
        self._connection.ioloop.add_callback(self._start_job, job, basic_deliver)

    async def run_subprocess(self, cmd):
        proc = await asyncio.create_subprocess_shell(
           cmd,
           stdout=asyncio.subprocess.PIPE,
           stderr=asyncio.subprocess.PIPE)

        stdout, stderr = await proc.communicate()

        if proc.returncode != 0:
            if stdout:
               LOGGER.warning(f'[stdout]\n{stdout.decode()}')
            if stderr:
               LOGGER.warning(f'[stderr]\n{stderr.decode()}')
        else:
            if stdout:
               LOGGER.info(f'[stdout]\n{stdout.decode()}')
            if stderr:
               LOGGER.info(f'[stderr]\n{stderr.decode()}')

        LOGGER.info(f'[{cmd!r} exited with {proc.returncode}]')

        return proc.returncode

    async def _download_job_data(self, job):
        # additional corpora can be used in training
        corpora = job["model"].get("additional_corpora", [])

        # determine what data to download
        download_speech = False
        download_text = False
        download_shared_models = False

        component = job["model"]["component"]["name"] if "component" in job["model"] else "main"

        if os.path.exists("/component-%s.yaml" % component):
            with open("/component-%s.yaml" % component, encoding="utf8") as f:
                component_conf = yaml.safe_load(f)
        else:
            component_conf = {}

        if component_conf.get("train_data_type", "auto") == "auto":
            if job["model"]["type"] == "ASR":
               download_speech = True
            else:
               download_text = True
        
        if component_conf.get("train_data_type", "auto") in ["speech", "both"]:
            download_speech = True

        if component_conf.get("train_data_type", "auto") in ["text", "both"]:
            download_text = True

        if component_conf.get("download_shared_models", False):
            download_shared_models = True

        # perform download
        if download_speech:
            out_dir = "/data/speech"
            await self.storage_service.download_speech_corpus(job["model"]["app_id"], out_dir)

            # download other corpora separately
            out_dir = "/data/add/speech"
            for c in corpora:
                await self.storage_service.download_speech_corpus(c["app_id"], out_dir)

        if download_shared_models:
            # download models from shared apps
            dbc = db.DB(self._etc_conf)
            for c in corpora:                
                shared_model = { 
                    "model": await dbc.get_latest_model(c["app_id"], job["model"]["type"])
                }
                await self._download_component_dependencies(shared_model, component_conf)

        if download_text:
            out_dir = "/data/text"
            await self.storage_service.download_text_corpus(job["model"]["app_id"], out_dir)

            # download other corpora separately
            out_dir = "/data/add/text"
            for c in corpora:
                await self.storage_service.download_text_corpus(c["app_id"], out_dir)

        await self._download_component_dependencies(job, component_conf)

    async def _download_component_dependencies(self, job, conf):
        LOGGER.info("Downloading dependencies")

        out_dir = "/data/deps"
        for dep in conf.get("deps", []):
            LOGGER.info("Downloading %s" % dep["name"])
            try:
                await self.storage_service.download_model_component(job["model"]["app_id"], 
                                                                    job["model"]["type"],
                                                                    job["model"]["_id"], 
                                                                    dep["name"], 
                                                                    out_dir,
                                                                    dep.get("save_as", None))
            except ResourceNotFoundError as e:
                if conf.get("download_shared_models", False):
                    # don't fail, depedency might come from other app
                    continue
                else:
                    # dependency could not be downloaded, fail
                    raise e

    async def _upload_model_components(self, job):
        # upload each model component as separate blob

        if not os.path.exists("/components"):
            return

        LOGGER.info("Uploading separate components")
        with os.scandir("/components") as it:
            for entry in it:
                if not entry.name.startswith('.') and entry.is_file():
                    with open("/components/%s" % entry.name, "rb") as blob:
                        await self.storage_service.upload_model_component(job["model"]["app_id"],
                                                    job["model"]["type"],
                                                    job["model"]["_id"],
                                                    entry.name,
                                                    blob)

    async def _model_job(self, job):
        dbc = db.DB(self._etc_conf)

        LOGGER.info("Training...")
        return_code = await self.run_subprocess("/job.sh %s" % job["app"]["language"])
 
        if return_code == 0:
            # upload model             
            LOGGER.info("Uploading trained model")

            # first upload main package
            with open("/model.mdl", "rb") as blob:
                await self.storage_service.upload_model(job["model"]["app_id"],
                                                        job["model"]["type"],
                                                        job["model"]["_id"],
                                                        blob)

            # upload model components
            await self._upload_model_components(job)

            # upload validation info
            LOGGER.info("Uploading validation info")
            with open("/validation.tgz", "rb") as blob:
                await self.storage_service.upload_model_valid(job["model"]["app_id"],
                                                    job["model"]["type"],
                                                    job["model"]["_id"],
                                                    blob)
            # model training successful
            await dbc.change_model_status(job["model"]["_id"], "training.ok")
        else:
            # model training failed
            await dbc.change_model_status(job["model"]["_id"], "training.failed")

        # upload logs            
        if os.path.exists("/logs.tgz"):
            LOGGER.info("Uploading logs")         
            with open("/logs.tgz", "rb") as blob:
                await self.storage_service.upload_model_logs(job["model"]["app_id"],
                                                             job["model"]["type"],
                                                             job["model"]["_id"],
                                                             blob)
        else:
            LOGGER.warning("Logs not found. Is this a bug?")

    async def _component_job(self, job):
        dbc = db.DB(self._etc_conf)

        # component jobs can have attachements
        if "attachement" in job:
           LOGGER.info("Job was submitted with attachement. Unpacking...")
           att_body = base64.b64decode(job["attachement"])
           with open("/data/attachement", "wb") as fd:
               fd.write(att_body)

        LOGGER.info("Retraining component %s..." % job["model"]["component"]["name"])
        return_code = await self.run_subprocess("/component.sh %s %s" % (job["app"]["language"], 
                                                                         job["model"]["component"]["name"]))

        if return_code == 0:
            # upload model components
            await self._upload_model_components(job)

            # model training successful
            await dbc.change_model_status(job["model"]["_id"], "training.ok")
        else:
            # model training failed
            await dbc.change_model_status(job["model"]["_id"], "training.failed")

        # upload logs            
        if os.path.exists("/logs.tgz"):
            LOGGER.info("Uploading logs")         
            with open("/logs.tgz", "rb") as blob:
                await self.storage_service.upload_model_logs(job["model"]["app_id"],
                                                             job["model"]["type"],
                                                             job["model"]["_id"],
                                                             blob)
        else:
            LOGGER.warning("Logs not found. Is this a bug?")

    async def _start_job(self, job, basic_deliver):
        dbc = db.DB(self._etc_conf)
        try:
            # acknowledge delivery of message
            self.acknowledge_message(basic_deliver.delivery_tag)

            # change status
            await dbc.change_model_status(job["model"]["_id"], "training.downloading_data")

            # download job
            LOGGER.info("Downloading training data")
            await self._download_job_data(job)

            # change status
            await dbc.change_model_status(job["model"]["_id"], "training.started")
        
            # run job
            if "component" in job["model"]:
                await self._component_job(job)
            else:
                await self._model_job(job)

        except KeyError:
            LOGGER.error("Training of non-existing model!! %s" % str(job))
        except Exception as e:
            LOGGER.error("Unexpected exception %s", e)
            await dbc.change_model_status(job["model"]["_id"], "training.failed")

        if self._consume_single:
            self.stop_consuming()
        else:
            self.start_consuming()

    def on_cancelok(self, unused_frame):
        """This method is invoked by pika when RabbitMQ acknowledges the
        cancellation of a consumer. At this point we will close the channel.
        This will invoke the on_channel_closed method once the channel has been
        closed, which will in-turn close the connection.

        :param pika.frame.Method unused_frame: The Basic.CancelOk frame

        """
        LOGGER.info('RabbitMQ acknowledged the cancellation of the consumer')
        self.close_channel()

    def on_pauseok(self, unused_frame):
        """This method is invoked by pika when RabbitMQ acknowledges the
        cancellation of a consumer.

        :param pika.frame.Method unused_frame: The Basic.CancelOk frame

        """
        LOGGER.info('RabbitMQ acknowledged the cancellation of the consumer')

    def pause_consuming(self):
        """Tell RabbitMQ that you would like to stop consuming by sending the
        Basic.Cancel RPC command.

        """
        self._consuming = False
        if self._channel:
            LOGGER.info('Sending a Basic.Cancel RPC command to RabbitMQ')
            self._channel.basic_cancel(self._consumer_tag, self.on_pauseok)

    def stop_consuming(self):
        """Tell RabbitMQ that you would like to stop consuming by sending the
        Basic.Cancel RPC command.

        """
        self._closing = True
        if self._channel:
            if self._consuming:
                LOGGER.info('Sending a Basic.Cancel RPC command to RabbitMQ')
                self._channel.basic_cancel(self._consumer_tag, self.on_cancelok)
            else:
                self.close_channel()
            

    def start_consuming(self):
        """This method sets up the consumer by first calling
        add_on_cancel_callback so that the object is notified if RabbitMQ
        cancels the consumer. It then issues the Basic.Consume RPC command
        which returns the consumer tag that is used to uniquely identify the
        consumer with RabbitMQ. We keep the value to use it when we want to
        cancel consuming. The on_message method is passed in as a callback pika
        will invoke when a message is fully received.

        """
        LOGGER.info('Issuing consumer related RPC commands')
        self._consuming = True
        self.add_on_cancel_callback()
        self._consumer_tag = self._channel.basic_consume(
            on_message_callback=self.on_message, queue=self.QUEUE)

    def on_bindok(self, unused_frame):
        """Invoked by pika when the Queue.Bind method has completed. At this
        point we will start consuming messages by calling start_consuming
        which will invoke the needed RPC commands to start the process.

        :param pika.frame.Method unused_frame: The Queue.BindOk response frame

        """
        LOGGER.info('Queue bound')
        if self._consuming:
            self.start_consuming()

    def close_channel(self):
        """Call to close the channel with RabbitMQ cleanly by issuing the
        Channel.Close RPC command.

        """
        LOGGER.info('Closing the channel')
        self._channel.close()

    def open_channel(self):
        """Open a new channel with RabbitMQ by issuing the Channel.Open RPC
        command. When RabbitMQ responds that the channel is open, the
        on_channel_open callback will be invoked by pika.

        """
        LOGGER.info('Creating a new channel')
        self._connection.channel(on_open_callback=self.on_channel_open)

    def run(self):
        """Run the example consumer by connecting to RabbitMQ and then
        starting the IOLoop to block and allow the SelectConnection to operate.

        """
        self._connection = self.connect()
        self._connection.ioloop.start()

    def stop(self):
        """Cleanly shutdown the connection to RabbitMQ by stopping the consumer
        with RabbitMQ. When RabbitMQ confirms the cancellation, on_cancelok
        will be invoked by pika, which will then closing the channel and
        connection. The IOLoop is started again because this method is invoked
        when CTRL-C is pressed raising a KeyboardInterrupt exception. This
        exception stops the IOLoop which needs to be running for pika to
        communicate with RabbitMQ. All of the commands issued prior to starting
        the IOLoop will be buffered but not processed.

        """
        LOGGER.info('Stopping')
        self.stop_consuming()
        self._connection.ioloop.start()
        LOGGER.info('Stopped')


def main():

    parser = argparse.ArgumentParser(description='Training job wrapper')
    parser.add_argument('-r', '--route', help='queue routing key', default = "jobs")
    parser.add_argument('-c', '--conf', help='config file', default = "config.yaml")
    parser.add_argument('-s', '--single', help='consume single message and exit', 
                        action='store_const', const=True, default = False)
    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    # load conf
    with open(args.conf, encoding="utf8") as f:
        conf = yaml.safe_load(f)

    consumer = JobConsumer(conf, consume_single = args.single, routing_key = args.route)
    try:
        consumer.run()
    except KeyboardInterrupt:
        consumer.stop()


if __name__ == '__main__':
    main()
