from typing import Union, Optional
import logging

logger = logging.getLogger(__name__)

# conversion utilities
def ensure_bytes(inp: Optional[Union[str, bytes]]) -> Optional[bytes]:
    if type(inp) == str:
        return inp.encode("utf-8")
    return inp


def ensure_str(inp: Optional[Union[str, bytes]]) -> Optional[str]:
    if type(inp) == bytes:
        logger.warning("ensure_str() called with content: %s" % str(inp))
        return inp.decode("utf-8")
    return inp
