from tornado.options import define


def define_global_settings() -> None:
    define("port", default=80, help="run on the given port", type=int)
    define("ssl_port", default=443, help="run on the given SSL port", type=int)
    define("max_filesize", default=100, help="max file length in megabytes", type=int)
    define("max_smallfilesize", default=1, help="max small (single utterance) file length in megabytes", type=int)
    define("conf", default='config.yaml', help="configuration", type=str)
    define("acme_dir", default='.well-known/acme-challenge', help="directory for acme challenages", type=str)
