# COMPRISE Cloud-based Platform

## Prerequisites

Cloud-based Platform depends on several external services that should be set up beforehand:
- OpenID Connect or OAuth2 authentication service (e.g. Azure B2C) 
- Cloud storage service (currently only support for Azure Blob storage is implemented)
- Mongo database service
- RabbitMQ message broker
- Kubernetes cluster

Configuration of these services is out of scope of this documentation, at minimum you will have to set up your
own cloud storage and authentication service. 

For quick start you can skip installation and just use a demo environment hosted at: https://comprise-dev.tilde.com/.
Currently approval of new accounts is manual, please contact us to get your account approved.

### Video installation guide
An installation & usage guide video can be found on YouTube [here](https://www.youtube.com/watch?v=swereu16oHg).

## Configuration

### API service

API service use config.yaml file for reading and storing configuration.

Please see config.yaml.example file which describes all configuration parameters.

### Web UI

Web UI configuration is stored in web-ui/src/environments/environment.prod.ts file.

> environment.prod.ts content description:
  - production: {true or false},
  - clientId: {OAuth2 client id}
  - redirectUri: {Web UI host uri}
  - tenantId: {omit, not used}
  - authority: {Uri for OAuth2 login page},
  - webApiUrl : { Uri for API service host, e.g. https://comprise-dev.tilde.com/v1alpha}

## Building Docker images

### Building API service image and job-wrapper image

```
git clone https://gitlab.inria.fr/comprise/comprise-cloud-based-platform
cd comprise-cloud-based-platform
sudo make docker job_docker
```

### Building Web UI image

```
git clone https://gitlab.inria.fr/comprise/comprise-cloud-based-platform
cd comprise-cloud-based-platform/web-ui
sudo docker build -t comprisedev.azurecr.io/comprise-web:latest .
```

## Deployment

### Localhost

It is possible to run COMPRISE Cloud platform API service and Web UI on a local computer. 

#### API service

- Prepare "config.yaml" configuration file in the current directory (use config.yaml.localhost as template)
- Prepare "user_roles.yaml" (see user_roles.yaml.example)
- Run MongoDB from Docker image as follows:
```
docker run -d -p 27017:27017 \
    -e MONGO_INITDB_ROOT_USERNAME=mongo \
    -e MONGO_INITDB_ROOT_PASSWORD=mongo \
    mongo
```
- Run rabbitmq from Docker image as follows:
```
docker run -d -p 5672:5672 \
    -e RABBITMQ_DEFAULT_USER=guest \
    -e RABBITMQ_DEFAULT_PASS=guest \
    rabbitmq:3
```
- Run the API service container using config.yaml from current working directory:
```
docker run \
  -v "$(pwd)":/etc/config \
  -p 5000:80 \
  comprisedev.azurecr.io/comprise-api-service:latest \
  --conf=/etc/config/config.yaml
```
- You can now use API service running on localhost:5000

#### Web UI

- Configure Web UI to use API from localhost. 
- Build Docker image as described above.
- Run the Web UI docker container:
```
docker run \
  -p 8080:80 \
  comprisedev.azurecr.io/comprise-web:latest 
```
- Open "http://localhost:8080/" in browser to use Web UI

### Kubernetes

The platform was designed to run in a Kubernetes cluster. However, because setting up a Kubernetes cluster 
is a non-trivial procedure specific to particular infrastructure and out-of-scope of COMPRISE, 
we include only example scripts and configuration files to install COMPRISE Cloud Platform 
inside an existing Kubernetes cluster.

NB: you should install and configure helm and kubectl before running commands below.
```
cd kubernetes
./install_cert_manager.sh  
./install_ingress.sh
./install_keda.sh
```

This will install:
- certificate manager, so that platform can work behind https endpoints.
- ingress controller (nginx), that will provide routing to platform services.
- KEDA and rabbitmq, a Kubernetes-based event-driven autoscaler and message broker for training job queueing. 

Directory "kubernetes/base" contains baseline configuration files that should be overriden by
files in one of the overlays in the "kubernetes/overlays".

In "kubernetes/overlays/example" you can find example configuration that sets up a API service 
and Web UI, fake ASR training job and enables https access to the cluster.

In order to start example configuration you should first edit settings in the kubernetes/overlays/example/config.yaml
and provide credentials to Azure Blob storage and services. You can also configure service to use file instead of Azure AD. 

Then Cloud platform can be deployed as follows:
```
kubectl apply -k kubernetes/overlays/example
```

## API documentation

COMPRISE Cloud service API documentation can be accessed in various ways:
- Via demo environment: https://comprise-dev.tilde.com/dashboard/api-documentation
- Via Web UI http://<webui-host>/dashboard/api-documentation
- Via API service: http://<api-host>/v1alpha/.well-known/service-desc (OpenAPI Specification in YAML format)
