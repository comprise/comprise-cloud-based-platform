#!/bin/sh

python3.7 utils/data_prep.py

sleep 300;


# Training recipe shall create 3 files
# 1) /model.mdl containing the trained model
# 2) /logs.tgz containing the log files
# 3) /validation.tgz containing some info on model validation (e.g. WER)

# Let's create some fake content
# model.mdl
ls /data/speech/* > /model.mdl
ls /data/add/speech >> /model.mdl
echo "FAKE JOB FOR LANG $1 TRAINING DONE" >> /model.mdl

# logs
tar -czf /logs.tgz /model.mdl

# validation
tar -czf /validation.tgz /model.mdl

# additional components can be prepared as follows:
mkdir -p /components
tar -czf /components/tdnn /model.mdl

