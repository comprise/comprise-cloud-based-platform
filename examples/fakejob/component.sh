#!/bin/sh

python3.7 utils/text_data_prep.py

# Training recipe shall create 3 files
# 1) /model.mdl containing the trained model
# 2) /logs.tgz containing the log files
# 3) /validation.tgz containing some info on model validation (e.g. WER)

# Let's create some fake content
# model.mdl
ls /data/speech/* > /model.mdl
ls /data/add/speech >> /model.mdl

# unpack dependency
cd /data/deps
tar -xzf am.tgz
ls /data/deps >> /model.mdl

cat /data/text-data-raw/text >> /model.mdl

cat /data/attachement >> /model.mdl

echo "FAKE JOB FOR COMPONENT $1 $2 DONE" >> /model.mdl

# package component
mkdir -p /components
tar -czf /components/$2 /model.mdl

